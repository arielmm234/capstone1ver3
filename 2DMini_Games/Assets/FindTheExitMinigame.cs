﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FindTheExitMinigame : MiniGameBase {
    public Animator anim;
    public List<Sprite> RightwayWalls;
    public List<Sprite> LeftwayWalls;
    public Sprite EndWall;
    Sprite curchoices;
    public SpriteRenderer MainWall;
    public float endtime;
    public bool isRight,Picked,end;
    public int Choices,wrongchoice;

    public override void OnEnable()
    {

        anim.gameObject.SetActive(false);
        Choices = 5;
        SetImage();
        end = false;
        endtime = 3;
        wrongchoice = 0;
        base.OnEnable();
    }
    public override void OnDisable()
    {
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (Choices == 0)
        {
            MainWall.sprite = EndWall;
            end = true;
            PauseGame();
            //Success();
        }
        if (currentTimer < 0 && !JustEnded)
        {
            Fail();
        }
        if (wrongchoice >= 3 && !JustEnded)
        {
            Fail();
        }
        if (Picked && Choices > 0)
        {
            Picked = false;
            SetImage();
        }
    }
    protected override void Update()
    {
        base.Update();
        if (end&& !JustEnded)
        {
            endtime -= Time.deltaTime;
        }
        if (endtime <= 0 && !JustEnded)
        {
            anim.gameObject.SetActive(true);
            anim.SetTrigger("Win");
        }
        if (end && endtime <= 0)
        {
            float animPerc = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = anim.GetCurrentAnimatorStateInfo(0).IsName("DuckWin");
            if (isWinAnim && animPerc > 1)
            {
                Debug.Log("yeah");
                end = false;
                Success();
            }
        }
    }
        
    public void SetImage()
    {
        int imagecount = LeftwayWalls.Count + RightwayWalls.Count;
        if (Random.Range(0, imagecount) > RightwayWalls.Count)
        {
            Sprite randomsprite = LeftwayWalls[Random.Range(0, LeftwayWalls.Count - 1)];
            if (randomsprite != curchoices)
            {
                MainWall.sprite = randomsprite;
                curchoices = randomsprite;
                isRight = false;
            }
            else SetImage();
        }
        else
        {
            Sprite randomsprite = RightwayWalls[Random.Range(0, RightwayWalls.Count - 1)];
            if (randomsprite != curchoices)
            {
                MainWall.sprite = randomsprite;
                curchoices = randomsprite;
                isRight = true;
            }
            else SetImage();
        }

    }
    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
    }

    public void PickedRight()
    {

        if (isRight)
        {
            Choices--;
            
        }
        else
        {
            Choices++;
            wrongchoice++;
        }
        Picked = true;

    }
    public void PickedLeft()
    {

        if (isRight)
        {
            Choices++;
            wrongchoice++;
        }
        else
        {
            Choices--;
        }
        Picked = true;
    }
}
