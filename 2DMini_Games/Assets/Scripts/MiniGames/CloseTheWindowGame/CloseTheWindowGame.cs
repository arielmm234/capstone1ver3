﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseTheWindowGame : MiniGameBase
{

	[Header ("Camera Variable")]

	public Camera VirtualCamera;
	public List<Window> Windows;
	public List<GameObject> WindowPlacement;
	public float TurnSpeed;
	public LayerMask LayerMask;
	public int windowsOpen;

	public Image LeftArrow;
	public Image RightArrow;

	//Can only be -1, 0, 1
	private int currentWall;

	// Use this for initialization
	public override void OnEnable ()
	{
		base.OnEnable ();
		var currentDiffData = currentDifficultyData as CloseTheWindowDifficulty;
		OpenWindow (currentDiffData);
        VirtualCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
        currentWall = 0;
        LeftArrow.gameObject.SetActive (true);
		RightArrow.gameObject.SetActive (true);
	}
	public override void OnDisable ()
	{
        base.OnDisable();
        for (int i = 0; i < Windows.Count; i++)
		{
			Windows[i].ResetWindow ();
		}
		LeftArrow.gameObject.SetActive (false);
		RightArrow.gameObject.SetActive (false);
	}

	public override void Reset ()
	{
		base.Reset ();
	}

	public override void RulesUpdate ()
	{
		base.RulesUpdate ();
		if (windowsOpen < 1)
		{
			Success ();
		}
	}

	public void OpenWindow (CloseTheWindowDifficulty difdata)
	{
		if (difdata.Difficulty == GameDifficulty.Tutorial)
		{
			WindowPlacement[0].SetActive (true);
			windowsOpen = 9;
		}
		if (difdata.Difficulty == GameDifficulty.Easy)
		{
			WindowPlacement[0].SetActive (true);
			WindowPlacement[1].SetActive (true);
			windowsOpen = 18;
		}
		if (difdata.Difficulty == GameDifficulty.Normal)
		{
			WindowPlacement[0].SetActive (true);
			WindowPlacement[1].SetActive (true);
			WindowPlacement[2].SetActive (true);
			windowsOpen = 27;
		}
	}
	// Update is called once per frame
	protected override void Update ()
	{
		base.Update ();
		TouchInput.SwipeData swipe = TouchInput.OnSwipe ();

		if (swipe.Phase == TouchInput.SwipePhase.Finished)
		{
			Vector2 normalizedDir = swipe.Direction.normalized;

			//If the swipe is horizontal
			if (Mathf.Abs (normalizedDir.x) > Mathf.Abs (normalizedDir.y))
			{
				//If the swipe is long enough
				if (swipe.Magnitude >= 60)
				{
					//Get the direction
					TurnCamera ((int) Mathf.Sign (normalizedDir.x));
				}
			}
			//If the swipe is vertical
			else
			{
				//Get the middle point of the swipe
				Vector2 middlePoint = Camera.main.ScreenToViewportPoint (Vector2.Lerp (swipe.StartPosition, swipe.EndPosition, 0.5f));

				Window nearest = (Windows.Count > 0) ? Windows[0] : null;
				Vector2 nearestPos = Vector2.zero;;

				if (nearest != null)
				{
					//Get nearest window from swipe
					for (int i = 0; i < Windows.Count; i++)
					{
						if (!IsWindowInView (Windows[i]))
						{
							continue;
						}

						nearestPos = Camera.main.WorldToViewportPoint (nearest.transform.position);
						Vector2 windowPos = Camera.main.WorldToViewportPoint (Windows[i].transform.position);

						float nearestDistance = (nearestPos - middlePoint).magnitude;
						float currentWindowDistance = (windowPos - middlePoint).magnitude;

						if (currentWindowDistance < nearestDistance)
						{
							nearest = Windows[i];
						}
					}
					if ((middlePoint - nearestPos).magnitude <= 0.35f && swipe.Magnitude > 40)
					{
						nearest.Close ();
					}
				}
			}
		}

		if (currentWall == -1)
		{
			LeftArrow.gameObject.SetActive (false);
			RightArrow.gameObject.SetActive (true);
		}
		else if (currentWall == 0)
		{
			LeftArrow.gameObject.SetActive (true);
			RightArrow.gameObject.SetActive (true);
		}
		else
		{
			LeftArrow.gameObject.SetActive (true);
			RightArrow.gameObject.SetActive (false);
		}

	}

	private bool IsWindowInView (Window window)
	{
		Vector3 screenPoint = Camera.main.WorldToViewportPoint (window.transform.position);
		return screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
	}

	public void TurnCamera (int direction)
	{
		int dir = (int) Mathf.Sign (direction);
		switch (dir)
		{
			case -1:
				if (currentWall != -1)
				{
					currentWall--;
					StartCoroutine (CameraLerp (direction));
				}
				break;
			case 1:
				if (currentWall != 1)
				{
					currentWall++;
					StartCoroutine (CameraLerp (direction));
				}
				break;
		}

	}

	private IEnumerator CameraLerp (int dir)
	{
		float angle = 0;

		while (angle < 90)
		{
			VirtualCamera.transform.Rotate (-Vector3.up, TurnSpeed * dir);
			angle += TurnSpeed;
			yield return new WaitForEndOfFrame ();
		}
	}
}