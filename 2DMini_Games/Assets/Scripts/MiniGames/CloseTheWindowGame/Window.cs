﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Window : MonoBehaviour
{

    public Sprite OpenSprite;
    public Sprite ClosedSprite;
    public CloseTheWindowGame game;
    public bool IsClosed;

    private SpriteRenderer spriteRenderer;
    private Sprite openSprite;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        openSprite = spriteRenderer.sprite;
    }

    public void Close()
    {
        if (IsClosed)
            return;

        spriteRenderer.sprite = ClosedSprite;
        game.windowsOpen--;
        IsClosed = true;
    }

    public void ResetWindow()
    {
        if (IsClosed)
        {
            spriteRenderer.sprite = openSprite;
            IsClosed = false;
        }
    }
}