﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class StartMenu : MonoBehaviour
{
    public ScriptableLife life;
    public GameObject LoadingBG;
    public GameObject TutorialPrompt;
    // Use this for initialization
    void Start()
    {
        life.life = 3;
        life.round = 0;
        AudioManager.Instance.Play("MainMenuBGM");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartGame(bool skipTutorial)
    {

        AudioManager.Instance.Stop("MainMenuBGM");
        life.life = 3;
        life.round = 0;

        UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
        LoadingBG.SetActive(true);
        if (skipTutorial)
            UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
        else
            UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");
    }
    public void ChoseGame(string gamename)
    {
        AudioManager.Instance.Stop("MainMenuBGM");
        UnityEngine.SceneManagement.SceneManager.LoadScene(gamename);
    }
    public void ExitGame()
    {

        AudioManager.Instance.Stop("MainMenuBGM");
        Application.Quit();
    }
}
