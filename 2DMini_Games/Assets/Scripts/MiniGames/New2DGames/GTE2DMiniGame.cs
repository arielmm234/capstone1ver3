﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GTE2DMiniGame : MiniGameBase
{
    public List<GameObject> RightItems, WrongItems, allItems, spots;
    public int CorrectItems, IncorrectItems;
    public Canvas mainCanvas;
    public GameObject Ingame;
    public Animator victory;
    public bool pickedRight, PickedWrong,won;


    public override void OnEnable()
    {
        AudioManager.Instance.Play("GetEssBG");
        base.OnEnable();
        victory.gameObject.SetActive(false);
        Ingame.SetActive(true);
        var currentDiffData = currentDifficultyData as ChoseTheEssentialsDificulty;
        CorrectItems = currentDiffData.CorrectItems;
        IncorrectItems = currentDiffData.IncorrectItems;
        Spawnitems();
    }
    public override void OnDisable()
    {
        
        AudioManager.Instance.Stop("GetEssBG");
        for (int i = 0; i < allItems.Count; i++)
        {
            Destroy(allItems[i]);
        }
        allItems.Clear();
        base.OnDisable();

    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (pickedRight)
        {
            pickedRight = false;
        }
        if (PickedWrong && !JustEnded)
        {
            PickedWrong = false;
            Fail();
        }
        if (CorrectItems < 1 && !JustEnded)
        {
            Ingame.SetActive(false);
            victory.gameObject.SetActive(true);
            won = true;
            PauseGame();
        }
        if (currentTimer <= 0 && !JustEnded)
        {
            Fail();
        }
    }
    protected override void Success()
    {
        base.Success();
    }
    protected override void Update()
    {
        base.Update();
         if (won)
        {
            float animPerc = victory.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = victory.GetCurrentAnimatorStateInfo(0).IsName("GetTheEssentialsWin");

            if (isWinAnim && animPerc > 1)
            {
                Debug.Log("yeah");
                won = false;
                Success();
            }

        }
    }

    public void Spawnitems()
    {
        int CorPos = CorrectItems;
        int Incorpos = IncorrectItems;
        for (int i = 0; i < CorrectItems + IncorrectItems; i++)
        {
            if (CorPos > 0)
            {
                if (Incorpos > 0)
                {
                    if (Random.Range(1, 3) < 2)
                    {
                        var CreatItem = Instantiate(RightItems[CorPos]) as GameObject;
                        CreatItem.transform.SetParent(mainCanvas.transform);
                        CreatItem.transform.position = spots[i].transform.position;
                        CreatItem.transform.localScale =new Vector3(32f, 32f, 32f);
                        CreatItem.transform.name = RightItems[CorPos].name;
                        CreatItem.transform.SetParent(Ingame.transform);
                        CorPos--;
                        allItems.Add(CreatItem);
                    }
                    else
                    {
                        var CreatItem = Instantiate(WrongItems[Incorpos]) as GameObject;
                        CreatItem.transform.SetParent(mainCanvas.transform);
                        CreatItem.transform.position = spots[i].transform.position;
                        CreatItem.transform.localScale =new Vector3(32f, 32f, 32f);
                        CreatItem.transform.name = WrongItems[Incorpos].name;
                        CreatItem.transform.SetParent(Ingame.transform);
                        Incorpos--;
                        allItems.Add(CreatItem);
                    }
                }
                else
                {
                    var CreatItem = Instantiate(RightItems[CorPos]) as GameObject;
                    CreatItem.transform.SetParent(mainCanvas.transform);
                    CreatItem.transform.position = spots[i].transform.position;
                    CreatItem.transform.localScale = new Vector3(32f, 32f, 32f);
                    CreatItem.transform.name = RightItems[CorPos].name;
                    CreatItem.transform.SetParent(Ingame.transform);
                    CorPos--;
                    allItems.Add(CreatItem);
                }
            }
            else if (Incorpos > 0)
            {
                var CreatItem = Instantiate(WrongItems[Incorpos]) as GameObject;
                CreatItem.transform.SetParent(mainCanvas.transform);
                CreatItem.transform.position = spots[i].transform.position;
                CreatItem.transform.localScale =new Vector3(32f, 32f, 32f);
                CreatItem.transform.name = WrongItems[Incorpos].name;
                CreatItem.transform.SetParent(Ingame.transform);
                Incorpos--;
                allItems.Add(CreatItem);
            }
        }
    }
    public void PickedRight()
    {
        pickedRight = true;
    }
    public void pickedWrong()
    {
        PickedWrong = true;
    }
}
