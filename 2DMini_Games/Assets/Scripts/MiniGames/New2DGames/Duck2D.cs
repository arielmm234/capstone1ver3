﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duck2D : MiniGameBase
{
    public Animator camAnim, PlayerAnim,WinAnim;
    public GameObject Player, Room;
    public bool PlayerMoveFromLeft,PlayerMoveToRight,RoomMove,CanMove;
    public bool Crouching;
    public int TimesItCanEarthQuake, TempTimesItCanEarthQuake;
    public bool IsEarthQuaking, JustCalledEarthquake;
    public float TimeUntilItEarthquakes;
    public float AmoutOfTimeOneCanStand;
    public float TimeOneIsStanding;
    public float AmountOfTimeItWillEarthQuake;
    public float AmountOftimeItsEarthquaking;
    bool isRunning,Justwin,justlose;
    
    public override void OnEnable()
    {

        AudioManager.Instance.Play("DuckWhenEarthquake");
        base.OnEnable();
        var currentDiffData = currentDifficultyData as DuckGameDificulty;
        TimesItCanEarthQuake = currentDiffData.Earthquack;
        TempTimesItCanEarthQuake = TimesItCanEarthQuake;
        AmountOfTimeItWillEarthQuake = currentDiffData.Quakingtime;
        AmoutOfTimeOneCanStand = currentDiffData.timeCanStandWhileQuaking;
        WinAnim.gameObject.SetActive(false);
        Player.transform.localPosition = new Vector3(-137,-38, -0.08f);
        Room.transform.localPosition = new Vector3(0,0,0);
        JustCalledEarthquake = false;
        TimeOneIsStanding = 0;
        CanMove = true;
        PlayerMoveFromLeft = true;
        Justwin = false;
        justlose = false;
        RoomMove = false;
        PlayerMoveToRight = false;
        TimeUntilItEarthquakes = Random.Range(1, initialTimer / TimesItCanEarthQuake);
    }
    public override void OnDisable()
    {

        AudioManager.Instance.Stop("DuckWhenEarthquake");
        base.OnDisable();
    }
    protected override void Update()
    {
        base.Update();
        if (Justwin && !JustEnded)
        {
            //DuckDefeat
            if (WinAnim.GetCurrentAnimatorStateInfo(0).IsName("DuckWin"))
            {
                float animPerc = WinAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;
                bool isWinAnim = WinAnim.GetCurrentAnimatorStateInfo(0).IsName("DuckWin");
                if (isWinAnim && animPerc > 1)
                {
                    Debug.Log("yeah");
                    Justwin = false;
                    Success();
                }
            }
           
        }
        if (justlose&& !JustEnded)
        {
            if (PlayerAnim.GetCurrentAnimatorStateInfo(0).IsName("DuckDefeat"))
            {
                PauseGame();
                float animPerc = PlayerAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;
                bool isWinAnim = PlayerAnim.GetCurrentAnimatorStateInfo(0).IsName("DuckDefeat");
                if (isWinAnim && animPerc > 1)
                {
                    Fail();
                }
            }
        }
    }

    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
        if (Input.GetMouseButton(0))
        {
            CanMove = false;
            Crouching = true;
            if (isRunning)
            {
                PlayerAnim.SetBool("Duck",true);
                PlayerAnim.SetBool("Run", false);
                isRunning = false;
            }
        }
        else
        {
            Crouching = false;
            CanMove = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            isRunning = true;
            PlayerAnim.SetBool("Duck", false);
            PlayerAnim.SetBool("Run", true);
        }
        
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (currentTimer <= 0)
        {
            //Fail();
            PlayerAnim.SetBool("Dead", true);
            PauseGame();
        }
        if (HasStarted && !GameOver)
        {
            //Movement
            if (CanMove)
            {
                if (PlayerMoveFromLeft)
                {
                    Player.transform.localPosition += new Vector3(1.5f, 0, 0);
                    if (Player.transform.localPosition.x > -1.0)
                    {
                        RoomMove = true;
                        PlayerMoveFromLeft = false;

                    }
                }
                if (RoomMove)
                {
                    Room.transform.localPosition -= new Vector3(1.5f, 0, 0);
                    if (Room.transform.localPosition.x < -1021)
                    {

                        PlayerMoveToRight = true;
                        RoomMove = false;
                    }
                }
                if (PlayerMoveToRight)
                {
                    Player.transform.localPosition += new Vector3(1.5f, 0, 0);
                    if (Player.transform.position.x > 136 && !Justwin)
                    {
                        camAnim.SetTrigger("Idle");
                        PlayerMoveToRight = false;
                        WinAnim.gameObject.SetActive(true);
                        WinAnim.SetTrigger("Win");
                        Justwin = true;
                        PauseGame();
                        //Success();
                    }
                }
            }
            if (IsEarthQuaking && !JustCalledEarthquake)
            {
                camAnim.SetTrigger("Shake");
                JustCalledEarthquake = true;
            }
            else
            {
                camAnim.SetTrigger("Idle");
                JustCalledEarthquake = false;
            }

            //EarthQuake
            if (!IsEarthQuaking && TimesItCanEarthQuake > 0)
            {
                if (TimeUntilItEarthquakes < 0)
                {
                    
                    IsEarthQuaking = true;
                    TimesItCanEarthQuake--;
                    AmountOftimeItsEarthquaking = AmountOfTimeItWillEarthQuake;
                    TimeUntilItEarthquakes = Random.Range(1, initialTimer / TempTimesItCanEarthQuake);
                    
                }
                TimeUntilItEarthquakes -= Time.deltaTime;
            }
            if (IsEarthQuaking)
            {
                
                AmountOftimeItsEarthquaking -= Time.deltaTime;
                if (!Crouching)
                {
                    TimeOneIsStanding += Time.deltaTime;
                    if (TimeOneIsStanding >= AmoutOfTimeOneCanStand && !JustEnded)
                    {
                        PlayerAnim.SetBool("Dead",true);
                        camAnim.SetTrigger("Idle");
                        justlose = true;
                        PauseGame();
                    }
                }
                if (AmountOftimeItsEarthquaking < 0)
                {
                    TimeOneIsStanding = 0;
                    AmountOftimeItsEarthquaking = AmountOfTimeItWillEarthQuake;
                    IsEarthQuaking = false;
                }
            }
            
        }
    }
   
}
