﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Dial2D911 : MiniGameBase
{
    public TextMeshProUGUI bubbletext;

    public Animator bubble;
    public List<int> EmergencyNumber, DialedNumber;
    public List<char> DialedNumberinWords;
    public TextMeshProUGUI NumberText;
    bool CorrectNumber, Checking;
    public int CorrectCount;
    bool wrongnumber;
    public override void OnEnable()
    {
        base.OnEnable();
        EmergencyNumber.Clear();
        DialedNumber.Clear();
        DialedNumberinWords.Clear();
        bubble.gameObject.SetActive(false);
        bubbletext.gameObject.SetActive(false);
        CorrectCount = 0;
        EmergencyNumber.Add(9);
        EmergencyNumber.Add(1);
        EmergencyNumber.Add(1);
        Checking = false;
        CorrectNumber = false;
        wrongnumber = false;
    }
    public override void OnDisable()
    {
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }
    protected override void Update()
    {
        base.Update();
        if (CorrectNumber && !JustEnded)
        {
            float animPerc = bubble.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = bubble.GetCurrentAnimatorStateInfo(0).IsName("BandageBubble");

            if (isWinAnim && animPerc > 1)
            {
                ClearNumber();
                bubbletext.gameObject.SetActive(true);
                bubbletext.text = "911 Whats your Emergency?";
                if (animPerc > 5)
                {
                    Success();
                }

            }
        }
        if (wrongnumber && !JustEnded)
        {
            float animPerc = bubble.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = bubble.GetCurrentAnimatorStateInfo(0).IsName("BandageBubble");

            if (isWinAnim && animPerc > 1)
            {
                ClearNumber();
                bubbletext.gameObject.SetActive(true);
                bubbletext.text = "Wrong Number";
                if (animPerc > 2)
                {

                    bubble.gameObject.SetActive(false);
                    bubbletext.gameObject.SetActive(false);
                    wrongnumber = false;
                }

            }
        }
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (currentTimer <= 0 && !JustEnded)
        {
            Fail();
        }
        if (Checking)
        {
            if (EmergencyNumber.Count == DialedNumber.Count)
            {
                for (int i = 0; i < EmergencyNumber.Count; i++)
                {

                    if (EmergencyNumber[i] != DialedNumber[i])
                    {
                        CorrectCount = 0;
                        Checking = false;
                        ClearNumber();
                        wrongnumber = true;

                        bubble.gameObject.SetActive(true);
                        return;
                    }
                    else
                    {
                        CorrectCount++;
                    }
                }
            }
            else
            {
                Checking = false;
                wrongnumber = true;

                bubble.gameObject.SetActive(true);
                ClearNumber();
                
                
            }
        }
        if (CorrectCount == EmergencyNumber.Count)
        {
            ClearNumber();
            CorrectNumber = true;
            bubble.gameObject.SetActive(true);
            PauseGame();
        }

        if (DialedNumber.Count > 3 && !wrongnumber)
        {

            AudioManager.Instance.Play("DiailTone");
            wrongnumber = true;
            bubble.gameObject.SetActive(true);
            ClearNumber();
        }
        NumberText.text = DisplayNumber();
    }
    public string DisplayNumber()
    {
        string results = "";
        foreach (var listmembers in DialedNumber)
        {
            results += listmembers.ToString();
        }
        return results;
    }
    public void ClearNumber()
    {
        DialedNumber.Clear();
    }
    public void DialNumber(int nunmber)
    {
        DialedNumber.Add(nunmber);
    }
    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
    }
    public void CheckNumber()
    {
        if (!JustEnded)
        {
            Checking = true;
            AudioManager.Instance.Play("911Answer");
            Debug.Log("win");
        }
       
        
    }
}
