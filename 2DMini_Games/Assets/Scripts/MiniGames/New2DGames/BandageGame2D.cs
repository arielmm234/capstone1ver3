﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BandageGame2D : MiniGameBase {
    public TextMeshProUGUI bubbletext;
    public Rigidbody Bandage;
    public Animator bubble;
    public List<GameObject> Points, Bandages;
    public int nextpoint,BandagePoint,bandages;
    public GameObject Arm;
    public float Maxspeed;
    public bool JustGrabed;
    bool ouch, excelent;
    public override void OnEnable()
    {
        base.OnEnable();

        AudioManager.Instance.Play("BandageBG");
        Bandage.gameObject.transform.localPosition = new Vector3(-250, -90, 0);
        bubble.gameObject.SetActive(false);
        bubbletext.gameObject.SetActive(false);
        nextpoint = 0;
        BandagePoint = 0;
        bandages = 0;
        JustGrabed = false;
        Points[0].SetActive(true);
        ouch = false;
        excelent = false;

    }
    public override void OnDisable()
    {

        AudioManager.Instance.Stop("BandageBG");
        for (int i = 0; i < Points.Count; i++)
        {
            Points[i].SetActive(false);
        }
        for (int i = 0; i < Bandages.Count; i++)
        {
            Bandages[i].SetActive(false);
        }
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }

    public override void RulesUpdate()
    {
        if (nextpoint < 12 && Vector3.Distance(Bandage.gameObject.transform.position, Points[nextpoint].transform.position) < 6)
        {
            Points[nextpoint].SetActive(false);
            if (Arm.transform.localPosition.z <= -1)
            {
                Arm.transform.localPosition = new Vector3(0, 0 ,1);
            }
            else
            {
                Arm.transform.localPosition = new Vector3(0, 0, -1);
            }
            nextpoint++;
            BandagePoint++;
            if(nextpoint < 12)
            {
                Points[nextpoint].SetActive(true);

            }
        }
        if (BandagePoint == 2)
        {
            AudioManager.Instance.Play("BandageStreach");
            bandages++;
            Bandages[bandages-1].SetActive(true);
            BandagePoint = 0;
        }
        if (currentTimer <= 0)
        {
            bubble.gameObject.SetActive(true);
            //Fail();

            ouch = true;
            PauseGame();
        }
        if (!JustEnded && Bandages[5].activeSelf)
        {
            //Success();
            bubble.gameObject.SetActive(true);
            bubbletext.text = "Excellent";
            AudioManager.Instance.Play("Thanks");
            excelent = true;
            PauseGame();
        }
        if (Bandage.velocity.magnitude >= Maxspeed && nextpoint > 0)
        {
            //Fail();
            bubble.gameObject.SetActive(true);
            ouch = true;
            AudioManager.Instance.Play("Ouch");
            PauseGame();
        }
        base.RulesUpdate();
    }
    protected override void Update()
    {
        base.Update();
        if ((excelent || ouch) &&!JustEnded)
        {
            float animPerc = bubble.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = bubble.GetCurrentAnimatorStateInfo(0).IsName("BandageBubble");

            if (isWinAnim && animPerc >1)
            {

                if (excelent)
                {
                    bubbletext.gameObject.SetActive(true);
                    if (animPerc > 3)
                    {
                        Success();
                    }

                }
                else
                {
                    bubbletext.gameObject.SetActive(true);
                    bubbletext.text = "Ouch! Too Fast!";
                    if (animPerc > 3)
                    {
                        Fail();
                    }

                }
                
            }
        }
    }
    public override void ControlsUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePosiion = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            Vector3 newPosition = Camera.main.ScreenToWorldPoint(mousePosiion);
            newPosition.z = 0;
            Bandage.MovePosition(newPosition);
            JustGrabed = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            JustGrabed = false;
        }
        base.ControlsUpdate();
    }

}
