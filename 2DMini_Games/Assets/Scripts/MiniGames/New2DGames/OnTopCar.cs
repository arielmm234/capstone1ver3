﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTopCar : MonoBehaviour {

    public DAPTSMinigame game;
    private void OnMouseOver()
    {
        game.OntopCar = true;
    }
    private void OnMouseExit()
    {
        game.OntopCar = false;
    }
}
