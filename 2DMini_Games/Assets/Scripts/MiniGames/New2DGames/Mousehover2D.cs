﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mousehover2D : MonoBehaviour {
    public DAPTSMinigame game;
    private void OnMouseOver()
    {
        game.OntopCar = true;
    }
    private void OnMouseExit()
    {
        game.OntopCar = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Blocker")
        {
            Debug.Log("hit");
            game.ResetCarPosition();
            //game.ResetCar();
        }
    }
}
