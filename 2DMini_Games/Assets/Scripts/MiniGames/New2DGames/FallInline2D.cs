﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallInline2D : MiniGameBase
{
    public List<GameObject> line, AddedPeople;
    public GameObject ManPrefab, WholeLine;
    public int NumOfPeople,LineAmount;
    public Transform topright, botleft;
    bool walking;
    public Canvas mainCanvas;

    public override void OnEnable()
    {
        base.OnEnable();
        AudioManager.Instance.Play("FallinLineBG");
        var currentDiffData = currentDifficultyData as FallInLineDificulty;
        WholeLine.transform.localPosition = new Vector3(-6.82f, 0,0);
        NumOfPeople = currentDiffData.NumOfPeople;
        line[0].SetActive(true);
        line[1].SetActive(true);
        for (int i = 0; i < NumOfPeople; i++)
        {
            var Person = Instantiate(ManPrefab);
            Person.transform.SetParent(mainCanvas.transform);
            Person.transform.localScale = new Vector2(1, 1);
            Person.transform.localPosition = new Vector3(Random.Range(botleft.position.x, topright.position.x), Random.Range(botleft.position.y, topright.position.x),-1);
            AddedPeople.Add(Person);
        }

    }
    public override void OnDisable()
    {
        // Debug.Log("disables");

        AudioManager.Instance.Stop("FallinLineBG");
        LineAmount = 0;
        for (int i = 0; i < AddedPeople.Count; i++)
        {
            Destroy(AddedPeople[i]);
        }
        AddedPeople.Clear();
        for (int i = 0; i < line.Count; i++)
        {
            line[i].SetActive(false);
        }
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (HasStarted && !GameOver)
        {
            if (LineAmount == NumOfPeople)
            {
                walking = true;
                for (int i = 0; i < line.Count; i++)
                {
                    line[i].GetComponent<Animator>().SetTrigger("Run");
                }
                PauseGame();
            }
            if (currentTimer <= 0)
            {
                Fail();
            }

        }
    }
    protected override void Update()
    {
        base.Update();
        if (walking)
        {
            WholeLine.transform.position += new Vector3(-1,0,0);
        }
        if (WholeLine.transform.localPosition.x <= -500 && !JustEnded)
        {
            walking = false;
            Success();
        }
    }
    protected override void Success()
    {
        base.Success();
    }
}
