﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayAwayGame : MiniGameBase {
    public Animator Playeranim,window1,window2;
    public GameObject Player, box, Shelf,Lamp,LampShadow;
    public Joystick Movement;
    public List<GameObject> first, second, third, fourth, fifth, sixth;
    [HideInInspector]
    public List<bool> firstbool, secondbool, thirdbool, fourthbool, fifthbool, sixthbool;
    int playerXpos, playerYpos;
    public float speed;
    int ShelfX, ShelfY,lampX,lampY;

    bool NotMoveing, fallendown, lightfallendown,gotHit,Nothit;
    public override void OnEnable()
    {
        base.OnEnable();
        Movement.gameObject.SetActive(true);
        setbool();
        playerXpos = 2;
        playerYpos = 2;
        Player.transform.localPosition = Setposition(playerXpos, playerYpos);
        
        ShelfX = Random.Range(0, 5);
        ShelfY = Random.Range(0, 5);
        lampX = Random.Range(1, 4);
        lampY = Random.Range(1, 4);
        gotHit = false;
        Nothit = false;
        placeShelf();
        placeLight();
        placeBox();
        fallendown = false;
        lightfallendown = false;
        Playeranim.SetBool("Lose", false);
        Playeranim.SetBool("Win", false);
        window1.SetBool("break", false);
        window2.SetBool("break", false);
        Shelf.transform.Find("RightShelf").GetComponent<Animator>().SetBool("Fall", false);
        Shelf.transform.Find("LEftShelf").GetComponent<Animator>().SetBool("Fall", false);
        LampShadow.transform.Find("LightShadow").gameObject.SetActive(true);
        Lamp.transform.Find("fixed").gameObject.SetActive(true);
        Lamp.transform.Find("broken").gameObject.SetActive(false);

    }
    public override void OnDisable()
    {

        Shelf.transform.Find("RightShelf").gameObject.SetActive(false);
        Shelf.transform.Find("LEftShelf").gameObject.SetActive(false);
        Movement.gameObject.SetActive(false);
        firstbool.Clear();
        secondbool.Clear();
        thirdbool.Clear();
        fourthbool.Clear();
        fifthbool.Clear();
        sixthbool.Clear(); 
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }
    protected override void Fail()
    {
            base.Fail();
    }
    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
        float step = speed * Time.deltaTime;
        Player.transform.localPosition = Vector3.MoveTowards(Player.transform.localPosition, Setposition(playerXpos, playerYpos), step);
        if (Player.transform.localPosition == Setposition(playerXpos, playerYpos))
        {
            NotMoveing = true;
            Playeranim.SetBool("Back", false);
            Playeranim.SetBool("Front", false);
        }
        if (Lamp.transform.localPosition == Setposition(lampX,lampY))
        {
            LampShadow.transform.Find("LightShadow").gameObject.SetActive(false);
            Lamp.transform.Find("fixed").gameObject.SetActive(false);
            Lamp.transform.Find("broken").gameObject.SetActive(true);
            SetBoolInstance(lampX, lampY);
            SetBoolInstance(lampX, lampY-1);
            SetBoolInstance(lampX+1, lampY);
            SetBoolInstance(lampX+1, lampY-1);
            if (ChecknextSquare(playerXpos, playerYpos))
            {
                lightfallendown = true;
            }
            else
            {
                gotHit = true;
            }
        }

        if (Movement.Vertical > 0.75 && playerYpos != 5 && NotMoveing && currentTimer > 0)
        {
            if (ChecknextSquare(playerXpos, playerYpos + 1))
            {
                playerYpos++;
                Playeranim.SetBool("Back",true);
                Playeranim.SetBool("Front", false);
                Playeranim.GetComponent<SpriteRenderer>().flipX = true;
                NotMoveing = false;
            }
        }
        else if (Movement.Vertical < -0.75 && playerYpos != 0 && NotMoveing && currentTimer > 0)
        {
            if (ChecknextSquare(playerXpos, playerYpos - 1))
            {
                playerYpos--;
                Playeranim.SetBool("Back", false);
                Playeranim.SetBool("Front", true);
                Playeranim.GetComponent<SpriteRenderer>().flipX = true;
                NotMoveing = false;
            }
        }
        else if (Movement.Horizontal > 0.75 && playerXpos != 5 && NotMoveing && currentTimer > 0)
        {
            if (ChecknextSquare(playerXpos + 1, playerYpos))
            {
                playerXpos++;
                Playeranim.SetBool("Back", false);
                Playeranim.SetBool("Front", true);
                Playeranim.GetComponent<SpriteRenderer>().flipX = false;
                NotMoveing = false;
            }
        }
        else if (Movement.Horizontal < -0.75 && playerXpos != 0 && NotMoveing && currentTimer > 0)
        {
            if (ChecknextSquare(playerXpos - 1, playerYpos))
            {
                playerXpos--;
                Playeranim.SetBool("Back", true);
                Playeranim.SetBool("Front", false);
                Playeranim.GetComponent<SpriteRenderer>().flipX = false;
                NotMoveing = false;
            }
        }
    }
    protected override void Update()
    {
        base.Update();
        if (Nothit)
        {
            float animPerc = Playeranim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = Playeranim.GetCurrentAnimatorStateInfo(0).IsName("StayAwayWin");
            if (isWinAnim && animPerc > 4 && !JustEnded && Nothit)
            {
                Nothit = false;
                Success();
            }
        }
        if (gotHit)
        {
            float animPerc = Playeranim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = Playeranim.GetCurrentAnimatorStateInfo(0).IsName("StayAwayFail");
            if (isWinAnim && animPerc > 4)
            {

                gotHit = false;
                Fail();
            }
        }
        if (fallendown)
        {
            if (Shelf.transform.Find("RightShelf").gameObject.activeSelf)
            {
                float animPerc = Shelf.transform.Find("RightShelf").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime;
                bool isWinAnim = Shelf.transform.Find("RightShelf").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("StayAwayShelfFall");
                if (isWinAnim && animPerc > 2)
                {
                    if (!ChecknextSquare(playerXpos, playerYpos))
                    {
                        gotHit = true;
                    }
                }
            }
            if (Shelf.transform.Find("LEftShelf").gameObject.activeSelf)
            {

                float animPerc = Shelf.transform.Find("LEftShelf").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime;
                bool isWinAnim = Shelf.transform.Find("LEftShelf").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("StayAwayShelfFall");
                if (isWinAnim && animPerc > 2)
                {
                    if (!ChecknextSquare(playerXpos, playerYpos))
                    {
                        gotHit = true;
                    }
                }
            }
        }
    }

    public override void RulesUpdate()
    {
        fall();
        if (!ChecknextSquare(playerXpos, playerYpos)&& !JustEnded)
        {
            //Fail();
            gotHit = true;
            Playeranim.SetBool("Lose",true);
        }
        if (currentTimer < 0 && !JustEnded)
        {
            window1.SetBool("break",true);
            window2.SetBool("break",true);
            SetBoolInstance(0,2);
            SetBoolInstance(1, 2);
            SetBoolInstance(0, 3);
            SetBoolInstance(1, 3);
            SetBoolInstance(0, 4);
            SetBoolInstance(1, 4);
            SetBoolInstance(4, 5);
            SetBoolInstance(3, 5);
            SetBoolInstance(3, 4);
            SetBoolInstance(4, 4);

        }
        if (lightfallendown && fallendown)
        {
            Nothit = true;
            Playeranim.SetBool("Win",true);
            PauseGame();
        }
        base.RulesUpdate();
    }
    public void fall()
    {
        if (currentTimer <= 0 && !fallendown)
        {
            if (Shelf.transform.Find("RightShelf").gameObject.activeSelf)
            {
                SetBoolInstance(ShelfX, 4);
                Shelf.transform.Find("RightShelf").GetComponent<Animator>().SetBool("Fall",true);
                fallendown = true;
            }
            if (Shelf.transform.Find("LEftShelf").gameObject.activeSelf)
            {

                SetBoolInstance(ShelfX + 1, ShelfY);
                Shelf.transform.Find("LEftShelf").GetComponent<Animator>().SetBool("Fall", true);
                fallendown = true;
            }
        }
        if (currentTimer <= 0)
        {
            Lamp.transform.localPosition = Vector3.MoveTowards(Lamp.transform.localPosition,Setposition(lampX,lampY),100*Time.deltaTime);
        }
    }
    public Vector3 Setposition(int X, int Y)
    {
        switch (Y)
        {
            case 0:
                return new Vector3(first[X].transform.localPosition.x, first[X].transform.localPosition.y, 2);
            case 1:
                return new Vector3(second[X].transform.localPosition.x, second[X].transform.localPosition.y, 2);
            case 2:
                return new Vector3(third[X].transform.localPosition.x, third[X].transform.localPosition.y, 2);
            case 3:
                return new Vector3(fourth[X].transform.localPosition.x, fourth[X].transform.localPosition.y, 2);
            case 4:
                return new Vector3(fifth[X].transform.localPosition.x, fifth[X].transform.localPosition.y, 2);
            case 5:
                return new Vector3(sixth[X].transform.localPosition.x, sixth[X].transform.localPosition.y, 2);
        }
        return new Vector3(0, 0, 0);

    }
    public bool ChecknextSquare(int x, int y)
    {
        if (!JustEnded)
        { 
        switch (y)
        {
            case 0:
                if (firstbool[x] == true) return true;
                else return false;
            case 1:
                if (secondbool[x] == true) return true;
                else return false;
            case 2:
                if (thirdbool[x] == true) return true;
                else return false;
            case 3:
                if (fourthbool[x] == true) return true;
                else return false;
            case 4:
                if (fifthbool[x] == true) return true;
                else return false;
            case 5:
                if (sixthbool[x] == true) return true;
                else return false;
            default:
                break;
        }
        }
        return true;
    }
    public void SetBoolInstance(int x, int y)
    {
        if (!JustEnded)
        {
            switch (y)
            {
                case 0:
                    firstbool[x] = false;
                    return;
                case 1:
                    secondbool[x] = false;
                    return;
                case 2:
                    thirdbool[x] = false;
                    return;
                case 3:
                    fourthbool[x] = false;
                    return;
                case 4:
                    fifthbool[x] = false;
                    return;
                case 5:
                    sixthbool[x] = false;
                    return;
            }
        }
    }
    public void setbool()
    {
        for (int i = 0; i < 6; i++)
        {
            bool Shit = true;
            firstbool.Add(Shit);
        }
        for (int i = 0; i < 6; i++)
        {
            bool Shit = true;
            secondbool.Add(Shit);
        }
        for (int i = 0; i < 6; i++)
        {
            bool Shit = true;
            thirdbool.Add(Shit);
        }
        for (int i = 0; i < 6; i++)
        {
            bool Shit = true;
            fourthbool.Add(Shit);
        }
        for (int i = 0; i < 6; i++)
        {
            bool Shit = true;
            fifthbool.Add(Shit);
        }
        for (int i = 0; i < 6; i++)
        {
            bool Shit = true;
            sixthbool.Add(Shit);
        }
    }
    public void placeBox()
    {
        if (ShelfX < 1)
        {
            
            int x = Random.Range(0, 5);
            int y = Random.Range(0, 5);
            if (x == 2 && y == 2)
            {
                placeBox();
            }
            else
            {
                if (ChecknextSquare(x - 1, y) && ChecknextSquare(x, y))
                {
                    box.transform.localPosition = Setposition(x, y);
                    SetBoolInstance(x, y);
                }
                else
                {
                    placeBox();
                }
            }
        }
        if (ShelfX > 0)
        {
            int x = Random.Range(0, 5);
            int y = Random.Range(0, 5);
            if (x == 2 && y == 2)
            {
                placeBox();
            }
            else
            {

                if (ChecknextSquare(x, y) && ChecknextSquare(x, y + 1))
                {
                    box.transform.localPosition = Setposition(x, y);
                    SetBoolInstance(x, y);
                }
                else
                {
                    placeBox();
                }
            }
        }
        
    }
    public void placeShelf()
    {
        if (ShelfX < 1)
        {
            Shelf.transform.Find("LEftShelf").gameObject.SetActive(true);
            Shelf.transform.localPosition = Setposition(ShelfX, ShelfY);
            SetBoolInstance(ShelfX, ShelfY);
        }
        else
        {
            Shelf.transform.Find("RightShelf").gameObject.SetActive(true);
            ShelfY = 5;
            Shelf.transform.localPosition = Setposition(ShelfX, ShelfY);
            SetBoolInstance(ShelfX, ShelfY);
        }
    }
    public void placeLight()
    {
        if (ShelfX < 1)
        {
            lampX = Random.Range(1, 4);
            lampY = Random.Range(1, 4);
            if (ChecknextSquare(lampX - 1, lampY) && ChecknextSquare(lampX, lampY))
            {
                
                Lamp.transform.localPosition = Setposition(lampX, lampY);
                Lamp.transform.localPosition += new Vector3(0, 200, 0);
                LampShadow.transform.localPosition = Setposition(lampX, lampY);

            }
            else
            {
                placeLight();
            }
        }
        
        if (ShelfX > 0)
        {
            lampX = Random.Range(1, 4);
            lampY = Random.Range(1, 4);
            if (ChecknextSquare(lampX, lampY) && ChecknextSquare(lampX, lampY))
            {
                
                Lamp.transform.localPosition = Setposition(lampX, lampY);
                Lamp.transform.localPosition += new Vector3(0, 200, 0);
                LampShadow.transform.localPosition = Setposition(lampX, lampY);
            }
            else
            {
                placeLight();
            }
        }

    }
}
