﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DTTSAGame : MiniGameBase
{
    public Animator camAnim,CarAnim;
    public Joystick Contorler;
    public GameObject Car,Road, CarBlock;
    public List<Sprite> blockersprite;
    public float speed, CarYpos;
    public int NumOfBlockers;
    public List<float> blockerPlacement;
    public List<GameObject> Blockers;
    bool RoadMove, CarMove;
    // road end -1066
    // end car 28
    public override void OnEnable()
    {
        base.OnEnable();
        AudioManager.Instance.Play("DriveToSafeArea");
        Contorler.gameObject.SetActive(true);
        Road.transform.localPosition = new Vector3(0, 0,2);
        CarYpos = -32;
        Car.transform.localPosition = new Vector3(0, CarYpos, 1);
        RoadMove = true;
        CarMove = false;
        PlaceBlockers();
    }
    public override void OnDisable()
    {

        AudioManager.Instance.Stop("DriveToSafeArea");
        Contorler.gameObject.SetActive(false);
        for (int i = 0; i < Blockers.Count; i++)
        {
            Destroy(Blockers[i]);
        }
        Blockers.Clear();
        blockerPlacement.Clear();
        base.OnDisable();

    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
        Car.transform.localPosition = new Vector3(Contorler.Horizontal * 100, CarYpos, 2);
        if (Contorler.Horizontal > 0f)
        {
            CarAnim.SetBool("GoingRight", true);
            CarAnim.SetBool("GoingLeft", false);
        }
        else if (Contorler.Horizontal < 0f)
        {
            CarAnim.SetBool("GoingLeft", true);
            CarAnim.SetBool("GoingRight", false);
        }
        else
        {
            CarAnim.SetBool("GoingRight", false);
            CarAnim.SetBool("GoingLeft", false);
        }
    }
    public void PlaceBlockers()
    {
        float SpaceBetween = 1095 / NumOfBlockers;
        for (int i = 0; i < NumOfBlockers + 1; i++)
        {
            blockerPlacement.Add(i * SpaceBetween);
        }
        for (int i = 1; i < NumOfBlockers; i++)
        {
            var Block = Instantiate(CarBlock);
            Block.transform.SetParent(Road.transform);
            Block.GetComponent<SpriteRenderer>().sprite = blockersprite[Random.Range(0, blockersprite.Count)];
            Block.transform.localPosition = new Vector3(Random.Range(-70f, 70f), Random.Range(blockerPlacement[i], blockerPlacement[i+1]), -1);
            Blockers.Add(Block);
        }
    }
    public void BlockerHit()
    {
            CarMove = false;
            RoadMove = false;
            camAnim.SetTrigger("Hit");
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (!CarMove && !RoadMove && !JustEnded)
        {
            float animPerc;
            bool isWinAnim;
            if (CarAnim.GetCurrentAnimatorStateInfo(0).IsName("DriveToSafeAreaCrashonRamp"))
            {
                Debug.Log("rapm");
               animPerc = CarAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;
               isWinAnim = CarAnim.GetCurrentAnimatorStateInfo(0).IsName("DriveToSafeAreaCrashonRamp");
                if (isWinAnim && animPerc > .98)
                {
                    Fail();
                    Debug.Log("yeah");
                    
                }
            }
            else
            {
                animPerc = CarAnim.GetCurrentAnimatorStateInfo(0).normalizedTime;
                isWinAnim = CarAnim.GetCurrentAnimatorStateInfo(0).IsName("DrivetosafeareaCarCrash");
                if (isWinAnim && animPerc > 3)
                {
                    Fail();
                }
            }
           
        }
        //road movement
        if (RoadMove)
        {
            Road.transform.localPosition -= new Vector3(0, speed, 0);
        }
        if (CarMove)
        {
            CarYpos += speed;
        }
        if (Road.transform.localPosition.y < -1066)
        {
            RoadMove = false;
            CarMove = true;
        }
        if (Car.transform.localPosition.y > 75 && !JustEnded)
        {
            CarMove = false;
            Success();
        }
    }
}
