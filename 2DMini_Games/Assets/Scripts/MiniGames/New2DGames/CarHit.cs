﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarHit : MonoBehaviour {

    public DTTSAGame game;
    public Sprite ramp;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Blocker")
        {
            game.BlockerHit();
            if(collision.GetComponent<SpriteRenderer>().sprite == ramp)
            {

                game.CarAnim.SetTrigger("RampCrash");
            }
            else
            {
                game.CarAnim.SetTrigger("CarCrash");
            }
        }
    }
}
