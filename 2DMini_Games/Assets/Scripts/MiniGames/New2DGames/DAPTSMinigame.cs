﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DAPTSMinigame : MiniGameBase
{
    float distance = 4;
    public GameObject Car,waypoint;
    public List<GameObject> marks;
    public bool OntopCar;
    bool arrived;

    public override void OnEnable()
    {
        base.OnEnable();
        ResetCarPosition();
        OntopCar = false;
        arrived = false;
    }
    public override void OnDisable()
    {
        ResetCarPosition();
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (currentTimer <= 0&& !JustEnded)
        {
            Fail();
        }
        if (Vector3.Distance(Car.transform.localPosition, waypoint.transform.localPosition) < 5  && !JustEnded && !arrived)
        {
            Success();
            arrived = true;
        }
        if (!JustEnded)
        {
            for (int i = 0; i < marks.Count; i++)
            {
                marks[i].transform.Rotate(0, 1, 0);
            }
        }
       
    }
    public void ResetCarPosition()
    {
       Car.transform.localPosition = new Vector3(144.9572f, 6.752186f, distance);
    }
    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
        if (Input.GetMouseButton(0) && OntopCar)
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            Car.transform.position = objPosition;
        }
    }
}
