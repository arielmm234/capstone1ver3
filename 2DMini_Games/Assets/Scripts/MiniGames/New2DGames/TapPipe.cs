﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapPipe : MonoBehaviour {

    public TapTheRightPipeMinigame game;

    public void OnMouseDown()
    {
        if (!game.lose && !game.won)
        {
            AudioManager.Instance.Play("PipeTap");
            if (this.gameObject.tag == "CorrectPipe")
            {
                game.TapedRightPipe();
            }
        }
    }
    
}
