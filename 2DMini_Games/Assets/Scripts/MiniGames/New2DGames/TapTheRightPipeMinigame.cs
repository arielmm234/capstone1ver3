﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapTheRightPipeMinigame : MiniGameBase
{
    public Animator anim;
    public Image progressBar;
    private float requiredProgress;
    private float progressPerClick;
    private float currentProgress;
    public List<GameObject> Pipes,ChatBubbles;
    public List<float> Xposition;
    float resetShuffle;
    public bool won, lose;
    

    public override void OnEnable()
    {
        base.OnEnable();
        anim.gameObject.SetActive(false);
        resetShuffle = 0;
        requiredProgress = 100;
        progressPerClick = 3;
        currentProgress = 0;
        progressBar.transform.parent.gameObject.SetActive(true);
        ShufflePipes();
        won = false;
        lose = false;
        ChatBubble();
    }
    public override void OnDisable()
    {
       
        currentProgress = 0;
        base.OnDisable();
        
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        resetShuffle += Time.deltaTime;
        if (resetShuffle >= 2)
        {
            ShufflePipes();
            ChatBubble();
            resetShuffle = 0;
        }
        if (currentProgress >= requiredProgress)
        {
            anim.gameObject.SetActive(true);
            ChatBubbles[0].SetActive(false);
            ChatBubbles[1].SetActive(false);
            ChatBubbles[2].SetActive(false);
            progressBar.transform.parent.gameObject.SetActive(false);
            anim.SetTrigger("Win");
            won = true; 
            PauseGame();
        }
        if (currentTimer <= 0)
        {
            anim.gameObject.SetActive(true);
            ChatBubbles[0].SetActive(false);
            ChatBubbles[1].SetActive(false);
            ChatBubbles[2].SetActive(false);
            progressBar.transform.parent.gameObject.SetActive(false);
            anim.SetTrigger("Lose");
            lose = true;
            PauseGame();
        }
        progressBar.fillAmount = currentProgress / requiredProgress;
        
    }
    protected override void Update()
    {
        base.Update();
        if (won)
        {
            float animPerc = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = anim.GetCurrentAnimatorStateInfo(0).IsName("TapTheRightPipeWin");
            if (isWinAnim && animPerc > 1)
            {
                Debug.Log("yeah");
                won = false;
                Success();
            }
        }
        if (lose)
        {
            float animPerc = anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = anim.GetCurrentAnimatorStateInfo(0).IsName("TapTheRightPipeDefeat");
            if (isWinAnim && animPerc > 1)
            {
                Debug.Log("yeah");
                lose = false;
                Fail();
            }
        }
    }
    public override void ControlsUpdate()
    {
        base.ControlsUpdate(); 
    }
    public void ChatBubble()
    {
        if (Pipes[0].transform.localPosition.x <= -30)
        {
            ChatBubbles[0].SetActive(true);
            ChatBubbles[0].GetComponent<Animator>().SetTrigger("Wake");
            ChatBubbles[1].SetActive(false);
            ChatBubbles[2].SetActive(false);
        }
        if (Pipes[0].transform.localPosition.x > -30 && Pipes[0].transform.localPosition.x < 30)
        {
            ChatBubbles[0].SetActive(false);
            ChatBubbles[1].SetActive(true);
            ChatBubbles[1].GetComponent<Animator>().SetTrigger("Wake");
            ChatBubbles[2].SetActive(false);
        }
        if (Pipes[0].transform.localPosition.x >= 30)
        {
            ChatBubbles[0].SetActive(false);
            ChatBubbles[1].SetActive(false);
            ChatBubbles[2].SetActive(true);
            ChatBubbles[2].GetComponent<Animator>().SetTrigger("Wake");
        }
    }
    public void ShufflePipes()
    {
        for (int i = 0; i < Xposition.Count; i++)
        {
            float temp = Xposition[i];
            int randomIndex = Random.Range(i, Xposition.Count);
            Xposition[i] = Xposition[randomIndex];
            Xposition[randomIndex] = temp;
        }
        for (int i = 0; i < 3; i++)
        {
            Pipes[i].transform.localPosition = new Vector3(Xposition[i], -62.29f, 0);
        }
    }
    public void TapedRightPipe()
    {
        if (Input.GetMouseButtonDown(0) && HasStarted)
        {
            currentProgress += progressPerClick;
        }
    }
}
