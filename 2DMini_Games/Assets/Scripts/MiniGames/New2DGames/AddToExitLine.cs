﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddToExitLine : MonoBehaviour {

    public FallInline2D game;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "2DMan")
        {
            Destroy(collision.gameObject);
            game.LineAmount++;
            game.line[game.LineAmount+1].SetActive(true);
        }
    }
}
