﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BandagePointManager : MonoBehaviour
{
    public GameObject Leg;
    public List<BandagePoint> Points = new List<BandagePoint>();
    LineRenderer lineRenderer;
    int lineRendererIndex;
    bool over;

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0, Points[0].gameObject.transform.position);
        lineRenderer.SetPosition(1, Points[0].gameObject.transform.position);
       // Points.ForEach(p => p.PointManager = this);
        for (int i = 1; i < Points.Count; i++)
            Points[i].gameObject.SetActive(false);
        lineRendererIndex = 1;
        over = true;
    }

    public void NextPoint(BandagePoint sender)
    {
        if (sender)
        {
            if (sender.TriggerTransparency)
            {
                Leg.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.4f);
                over = false;
            }
            else
            {
                Leg.GetComponent<Renderer>().material.color = Color.white;
                over = true;
            }
            if (sender.ConnectLine)
            {
                float distance = (lineRenderer.GetPosition(lineRendererIndex - 1) - sender.gameObject.transform.position).magnitude;
                Vector3 middle = Vector3.MoveTowards(lineRenderer.GetPosition(lineRendererIndex - 1), sender.gameObject.transform.position, distance / 2);
                middle.z = over ? 80 : 120f;
                lineRenderer.positionCount += lineRendererIndex == 1 ? 0 : 1;
                lineRenderer.SetPosition(lineRendererIndex, middle);
                lineRendererIndex++;
                lineRenderer.positionCount++;
                lineRenderer.SetPosition(lineRendererIndex, Points[0].transform.position);
                lineRendererIndex++;
            }
            Points.RemoveAt(Points.FindIndex(p => p.Equals(sender)));
            Destroy(sender.gameObject);
            Points.TrimExcess();
            if (Points.Count > 0)
                Points[0].gameObject.SetActive(true);
        }
        else
        {
            Points.Clear();
            Points.TrimExcess();
            
        }
    }
}
