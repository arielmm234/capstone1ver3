﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BandagePoint : MonoBehaviour
{
    public bool TriggerTransparency;
    public bool ConnectLine;
    public bool LastPoint;
    [HideInInspector]
    public BandageGame PointManager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BandageDrag>())
        {          
            PointManager.NextPoint(this);
            if (LastPoint)
                this.gameObject.SetActive(false);
        }
    }
}
