﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class TrimmTheBranchesDifficulty : MinigameDifficulty
{
    public int NumberOfBranches;
    public float RotationSpeed;
}
