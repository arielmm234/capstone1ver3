﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Emergency Broadcast Difficulty")]
public class EmergencyBroadcastDifficulty : MinigameDifficulty {

    public float StationCaptureRange;
    public float audioOffset;
    public float timeOnStation;

}
