﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class ChoseTheSafePathDificulty : MinigameDifficulty
{
    public int Rounds;
	
}
