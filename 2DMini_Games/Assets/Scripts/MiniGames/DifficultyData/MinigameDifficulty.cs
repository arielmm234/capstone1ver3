﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GameDifficulty {
    Tutorial    = 0,
    Easy        = 1,
    Normal      = 2,
    Hard        = 3,
    Boss        = 4,
}
public enum GameType {

    Earth = 0,
    Storm = 1,
}
public class MinigameDifficulty : ScriptableObject {
    public GameDifficulty Difficulty;
    public GameType TypeOfGame;
    public float time;
	
}
