﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmergencyBroadcastGame : MiniGameBase
{

    public GameObject RadioDial;
    public GameObject StationIndicator;

    public AudioSource WhiteNoise;
    public AudioSource ActualRadio;

    public float StationCaptureRange;
    public float AudioOffset;

    public float TimeOnStationTillSuccess;
    public bool StayingOnStation;
    public bool Won;

    public Vector2 Stations;

    public float SelectedStation;

    public List<float> PossibleStations;

    public override void OnEnable()
    {

        var currentDiffData = currentDifficultyData as EmergencyBroadcastDifficulty;
        TimeOnStationTillSuccess = currentDiffData.timeOnStation;
        AudioOffset = currentDiffData.audioOffset;
        StationCaptureRange = currentDiffData.StationCaptureRange;
        base.OnEnable();
        SelectedStation = Random.Range(Stations.y + .75f, Stations.x - .75f);
        StayingOnStation = false;
        Won = false;
    }
    public override void OnDisable()
    {
        base.OnDisable();

    }
    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
        TouchInput.SwipeData swipe = TouchInput.OnSwipe();

        Vector3 rotation = Vector3.zero;
        Vector3 desiredRot = Vector3.zero;
        if (swipe.Touch != null)
        {
            if (swipe.Phase == TouchInput.SwipePhase.Start)
            {
                rotation = RadioDial.transform.eulerAngles;
            }
            if (swipe.Phase == TouchInput.SwipePhase.Swiping)
            {
                Ray ray = Camera.main.ScreenPointToRay(swipe.StartPosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (hit.collider.CompareTag("RadioDial"))
                    {
                        AudioManager.Instance.Play("Radio Dial");

                        Vector2 dir = swipe.EndPosition - swipe.StartPosition;
                        float sign = Mathf.Sign(dir.x);

                        desiredRot = rotation + new Vector3(180, 0, (dir.magnitude * -1.5f) * sign);

                        RadioDial.transform.eulerAngles = desiredRot;

                        Vector3 indicatorPos = new Vector3(Mathf.Lerp(Stations.x, Stations.y, (desiredRot.z + 180) / 360), StationIndicator.transform.position.y, StationIndicator.transform.position.z);
                        // Debug.Log();
                        StationIndicator.transform.position = indicatorPos;
                    }
                }
            }
        }
        AdjustVolume();
    }
    public override void RulesUpdate()
    {
        if (GotRadioStation())
        {
            if (!StayingOnStation && !Won)
            {
                StartCoroutine(StayedOnStation(TimeOnStationTillSuccess));
                StayingOnStation = true;
            }
        }
        else
        {
            if (StayingOnStation)
            {
                StayingOnStation = false;
                StopAllCoroutines();
            }
        }
    }
    protected override void Update()
    {
        base.Update();
        if (WhiteNoise.mute == true && ActualRadio.mute == true && !JustStarted && !Won)
        {
            WhiteNoise.mute = false;
            ActualRadio.mute = false;
        }
    }

    private void AdjustVolume()
    {
        float indicatorDistance = StationIndicator.transform.position.x - SelectedStation;

        float indicatorMagnitude = (indicatorDistance) / ((Stations.y * -1) + Stations.x);

        float clampedMagnitude = Mathf.Clamp(indicatorMagnitude, -0.75f, 0.75f);

        float actualRadioVolume = 0;

        WhiteNoise.volume = Mathf.Abs(clampedMagnitude);

        if (Mathf.Abs(clampedMagnitude) > (1 - StationCaptureRange) - AudioOffset)
        {
            actualRadioVolume = 0.75f - WhiteNoise.volume;
            actualRadioVolume = Mathf.Clamp(actualRadioVolume, 0.0f, 0.15f);
        }
        else if (Mathf.Abs(clampedMagnitude) < StationCaptureRange + AudioOffset)
        {
            actualRadioVolume = 0.75f - WhiteNoise.volume;
            actualRadioVolume = Mathf.Clamp(actualRadioVolume, 0.0f, 0.75f);
        }

        //Debug.Log(clampedMagnitude);

        ActualRadio.volume = Mathf.Abs(actualRadioVolume);
    }

    public bool GotRadioStation()
    {
        return Mathf.Abs(StationIndicator.transform.position.x - SelectedStation) <= StationCaptureRange;
    }

    IEnumerator StayedOnStation(float delay)
    {
        yield return new WaitForSeconds(delay);
        WhiteNoise.mute = true;
        ActualRadio.mute = true;
        Success();
        StayingOnStation = false;
        Won = true;
        Debug.Log("99");
    }
}