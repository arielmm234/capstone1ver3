﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontPressTheButtonGame : MiniGameBase {

    bool PickedRight;
    public Animator elevanim;
    public GameObject elevator;
    public bool won,lost,crashed;
    public override void OnEnable()
    {
        base.OnEnable();
        elevator.SetActive(true);
        PickedRight = false;
        won = false;
        lost = false;
        crashed = false;
        
    }
    protected override void Update()
    {
        base.Update();
        if (currentTimer <= 0  && !JustEnded)
        {
            won = true;
            elevanim.SetTrigger("Win");
            PauseGame();
        }
        if (won)
        {
            float animPerc = elevanim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = elevanim.GetCurrentAnimatorStateInfo(0).IsName("DontPressButtonWin");

            if (isWinAnim && animPerc > 1 && !JustEnded)
            {
                Debug.Log("yeah");
                Success();
            }

        }
        if (lost)
        {
            float animPerc = elevanim.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool isWinAnim = elevanim.GetCurrentAnimatorStateInfo(0).IsName("DontPressButtonDefeat");
            if (isWinAnim && animPerc > .7f && !crashed)
            {
                AudioManager.Instance.Play("ElevatorCrash");
                crashed = true;
            }
            if (isWinAnim && animPerc > 1 && !JustEnded)
            {
                Fail();
            }
        }
    }
    public void Clicked()
    {
        if (PickedRight == false)
        {
            AudioManager.Instance.Play("ElevatorOpen");
            elevator.SetActive(false);
            PickedRight = true;
            elevanim.SetTrigger("Defeat");
            lost = true;
            PauseGame();
        }
    }
    
    public override void OnDisable()
    {
        elevator.SetActive(false);
        base.OnDisable();
    }
    public override void ControlsUpdate()
    {
        
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        
    }
}
