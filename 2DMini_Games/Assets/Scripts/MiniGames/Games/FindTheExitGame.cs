﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindTheExitGame : MiniGameBase {

    public Joystick control,eyes;
    public GameObject controls;
    public CharacterController player;
    public Cinemachine.CinemachineVirtualCamera cameraa;
    public float HorizontalRotation, PlayerHorizontalRotation, MovementSpeed,eyespeed;
    float VerticalRotation = 0;
    public List<GameObject> levels;
    public List<GameObject> FireExit;
    int level;
    bool GotOut;

    public override void OnEnable()
    {
        
        base.OnEnable();
        var currentDiffData = currentDifficultyData as FTEDiffivulty;
        player.transform.position = new Vector3(0, 0.77f, 0);
        player.transform.rotation = Quaternion.Euler(0, 0, 0);
        HorizontalRotation = 0;
        cameraa.transform.rotation = Quaternion.Euler(0, 0, 0);
        controls.SetActive(true);
        FireExit[returnlevel(currentDifficultyData)].SetActive(true); ;
        Setlevel(currentDiffData);
        GotOut =  false;
        level = returnlevel(currentDiffData);
        
    }
    public override void OnDisable()
    {
        base.OnDisable();
        controls.SetActive(false);
        player.transform.rotation = Quaternion.Euler(0, 0, 0);
        cameraa.transform.localRotation = Quaternion.Euler(0, 0, 0);
        for (int i = 0; i < levels.Count; i++)
        {
            levels[i].SetActive(false);
            FireExit[i].SetActive(false);
        }
        
    }
    public override void ControlsUpdate()
    {
        cameraa.transform.position = player.transform.position;
        //Camera Rotation
        PlayerHorizontalRotation = eyes.Horizontal * Time.deltaTime * 50.0f; 
        HorizontalRotation += eyes.Horizontal * Time.deltaTime * eyespeed;
        //VerticalRotation += -eyes.Vertical * Time.deltaTime * eyespeed;
        //VerticalRotation = Mathf.Clamp(VerticalRotation, -30, 30);
        cameraa.transform.localRotation = Quaternion.Euler(VerticalRotation, HorizontalRotation, 0);
       
        //Movement
        player.transform.Rotate(0, PlayerHorizontalRotation, 0);
        float ForwardSpeed = control.Vertical * Time.deltaTime * MovementSpeed;
        float SideSpeed = control.Horizontal * Time.deltaTime * MovementSpeed;
        Vector3 speed = new Vector3(SideSpeed, 0, ForwardSpeed);
        speed = player.transform.rotation * speed;
        
        player.Move(speed * Time.deltaTime);
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        if (Vector3.Distance(FireExit[level].transform.position, player.transform.position) < 3&& !GotOut)
        {
            GotOut = true;
            Success();
        }
    }
    public void Setlevel(MinigameDifficulty lvldif)
    {
        if (lvldif.Difficulty == GameDifficulty.Easy || lvldif.Difficulty == GameDifficulty.Tutorial)
        {
            levels[0].SetActive(true);
        }
        if (lvldif.Difficulty == GameDifficulty.Normal)
        {
            levels[1].SetActive(true);
        }
        if (lvldif.Difficulty == GameDifficulty.Hard)
        {
            levels[2].SetActive(true);
        }
    }

    public int returnlevel(MinigameDifficulty lvldif)
    {
        if (lvldif.Difficulty == GameDifficulty.Easy|| lvldif.Difficulty == GameDifficulty.Tutorial)
        {
            return 0;
        }
        if (lvldif.Difficulty == GameDifficulty.Normal)
        {
            return 1;
        }
        if (lvldif.Difficulty == GameDifficulty.Hard)
        {
            return 2;
        }
        return 4;
    }
}
