﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallInLineGame : MiniGameBase
{

    [Header("Fall In Line Variable")]
    public List<GameObject> line;
    public List<GameObject> AddedPeople;

    public int lineAmount;
    public GameObject People,WholeLine;
    
    public int NumOfPeople;
    public bool reset,walking;

    public override void OnEnable()
    {
        base.OnEnable();
        var currentDiffData = currentDifficultyData as FallInLineDificulty;
        line[0].SetActive(true);
        line[1].SetActive(true);
        WholeLine.transform.position = new Vector3(-6.48f,0, 2.73f);
        NumOfPeople = currentDiffData.NumOfPeople;
        for (int i = 0; i < NumOfPeople; i++)
        {
            var Person = Instantiate(People, new Vector3(Random.Range(-4, 7), 0.0f, Random.Range(-4, 4)), Quaternion.identity);
            Person.transform.Rotate(-36.983f, 180,0);
            AddedPeople.Add(Person);
        }

    }
    public override void OnDisable()
    {
        base.OnDisable();
        lineAmount = 0;
        for (int i = 0; i < AddedPeople.Count; i++)
        {
            Destroy(AddedPeople[i]);
        }
        AddedPeople.Clear();
        for (int i = 0; i < line.Count; i++)
        {
            line[i].SetActive(false);
        }
    }
    public override void Reset()
    {
        base.Reset();
       
    }
    public override void RulesUpdate()
	{
        if (HasStarted && !GameOver)
        {
            if (lineAmount == NumOfPeople)
            {
                for (int i = 0; i < line.Count; i++)
                {
                    //line[i].GetComponent<Animator>().SetTrigger("Walking");
                }
                walking = true;
                PauseGame();
            }

        }
    }
    protected override void Update()
    {
        base.Update();
        if (walking)
        {
            WholeLine.transform.position += new Vector3(0,0,-0.041f);
        }
        if (WholeLine.transform.position.z <= -5 && walking)
        {
            walking = false;
            Success();
            
        }
    }
    protected override void Success()
    {
        base.Success();
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.tag == "Person")
    //    {
    //        Destroy(other.gameObject);
    //        line[lineAmount+2].SetActive(true);
    //        lineAmount++;
    //    }
    //}
    
    

}
