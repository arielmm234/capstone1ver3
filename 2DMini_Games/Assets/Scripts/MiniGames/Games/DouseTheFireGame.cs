﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DouseTheFireGame : MiniGameBase
{
    public List<Sprite> CorWoodImage, IncorWoodImage, CorElecImage, IncorElecImage;
    public int Rounds;
    public bool isWood;
    public Image onFire;
    public Sprite tv,table;
    public bool clickedRight;
    public bool clickedWrong;
    public GameObject canvas;
    public GameObject CorChoice, IncChoice;
    public List<Transform> placement;
    public List<GameObject> numbers,Fires;
    float xpos;
    public override void OnEnable()
    {
        base.OnEnable();
        onFire.gameObject.SetActive(true);
        AudioManager.Instance.Play("DouseBG");
        var currentDiffData = currentDifficultyData as DouseTheFireDificulty;
        Camera.main.transform.position = new Vector3(0, 0, 0);
        canvas.GetComponent<Canvas>();
        Rounds = currentDiffData.Rounds;
        for (int i = 0; i < Rounds; i++)
        {
            Fires[i].SetActive(true);
        }
        if (Random.Range(0, 100) > 50)
        {
            isWood = true;
            onFire.sprite = table;
        }
        else
        {
            isWood = false;
            onFire.sprite = tv;
        }
        CreatChoices();
        clickedWrong = false;
        clickedRight = false;

    }
    public override void OnDisable()
    {

        AudioManager.Instance.Stop("DouseBG");
        if (numbers.Count > 0)
        {
            for (int i = 0; i < 3; i++)
            {
                Destroy(numbers[i]);
            }
            numbers.Clear();
        }
        base.OnDisable();
    }
    public override void ControlsUpdate()
    {

    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        if (clickedWrong && !GameOver)
        {
            Fail();
        }
        if (currentTimer <= 0 && !JustEnded)
        {
            Fail();
        }
        if (clickedRight)
        {
            clearImages();
            if (Rounds < 1)
            {
                Success();
            }
            else
            {
                Fires[Rounds - 1].SetActive(false);
                CreatChoices();
                clickedRight = false;
                Rounds--;
                if (Rounds < 1)
                {
                    Success();
                }
            }

        }
    }
    public void ClickedCorrect()
    {
        clickedRight = true;
    }
    public void ClickedIncorrect()
    {
        clickedWrong = true;
    }
    public void CreatChoices()
    {
        int CorPos = 1;
        int Incorpos = 2;
        for (int i = 0; i < 3; i++)
        {
            xpos = placement[i].position.x;
            if (CorPos > 0)
            {
                if (Incorpos > 0)
                {
                    if (Random.Range(1, 3) < 2)
                    {
                        if (isWood)
                        {
                            SpawnImage(CorChoice, canvas, ref CorPos, xpos, CorWoodImage);
                        }
                        if (!isWood)
                        {
                            SpawnImage(CorChoice, canvas, ref CorPos, xpos, CorElecImage);
                        }
                    }
                    else
                    {
                        if (isWood)
                        {
                            SpawnImage(IncChoice, canvas, ref Incorpos, xpos, IncorWoodImage);
                        }
                        if (!isWood)
                        {
                            SpawnImage(IncChoice, canvas, ref Incorpos, xpos, IncorElecImage);
                        }
                    }
                }
                else
                {
                    if (isWood)
                    {
                        SpawnImage(CorChoice, canvas, ref CorPos, xpos, CorWoodImage);
                    }
                    if (!isWood)
                    {
                        SpawnImage(CorChoice, canvas, ref CorPos, xpos, CorElecImage);
                    }
                }
            }
            else if (Incorpos > 0)
            {
                if (isWood)
                {
                    SpawnImage(IncChoice, canvas, ref Incorpos, xpos, IncorWoodImage);
                }
                if (!isWood)
                {
                    SpawnImage(IncChoice, canvas, ref Incorpos, xpos, IncorElecImage);
                }
            }

        }
    }

    public void SpawnImage(GameObject choice, GameObject canvass, ref int typechoice, float Xposition, List<Sprite> Imagess)
    {
        GameObject creatImage = Instantiate(choice);
        creatImage.transform.SetParent(canvass.transform, false);
        if (choice == IncChoice)
        {
            creatImage.GetComponent<Image>().sprite = Setimage(typechoice,Imagess);
        }
        else
        {
            creatImage.GetComponent<Image>().sprite = Imagess[Random.Range(0, Imagess.Count)];
        }
        creatImage.GetComponent<Image>().sprite = Imagess[Random.Range(0, Imagess.Count)];
        creatImage.transform.position = new Vector3(Xposition, placement[0].position.y, canvass.transform.position.z);
        typechoice--;
        creatImage.transform.SetSiblingIndex(5);
        numbers.Add(creatImage);
    }
    public void clearImages()
    {
        for (int i = 0; i < 3; i++)
        {
            Destroy(numbers[i]);
        }
        numbers.Clear();
    }
    Sprite Setimage(int typechoice, List<Sprite> Imagess)
    {
        Sprite randomsprite = Imagess[Random.Range(0, Imagess.Count - 1)];
        if (typechoice == 1)
        {
            for (int i = 0; i < numbers.Count; i++)
            {
                if (randomsprite == numbers[i].GetComponent<Image>().sprite)
                {
                    randomsprite =  Setimage(typechoice, Imagess);
                }
            }
            return randomsprite;
        }
        return randomsprite;
    }
}
