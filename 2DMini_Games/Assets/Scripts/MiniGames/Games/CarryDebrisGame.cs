﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CarryDebrisGame : MiniGameBase
{
    [Header ("Carry Debris Variables")]
    public Animator PlayerAnimator;
    public GameObject HeldRubble;

    [Header ("Carry Debris UI")]
    public Image progressBar;

    private float requiredProgress;
    private float progressReductionPerSecond;
    private float progressPerClick;
    private float currentProgress;
    private bool triggeredWinAnim;

    private bool won;

    public override void OnEnable ()
    {
        base.OnEnable ();
        won = false;
        var currentDiffData = currentDifficultyData as CarryDebrisDiffcultyBase;
        requiredProgress = currentDiffData.RequiredProgress;
        progressReductionPerSecond = currentDiffData.ProgressReductionPerSecond;
        progressPerClick = currentDiffData.ProgressPerClick;
        progressBar.transform.parent.gameObject.SetActive (true);
        currentProgress += 30.0f;

        if (!HeldRubble.activeSelf)
            HeldRubble.SetActive (true);

    }
    public override void OnDisable ()
    {
        base.OnDisable ();
        progressBar.transform.parent.gameObject.SetActive (false);
        currentProgress = 0;
        triggeredWinAnim = false;
    }
    public override void ControlsUpdate ()
    {
        if (Input.GetMouseButtonDown (0) && HasStarted)
            currentProgress += progressPerClick;

    }
    public override void RulesUpdate ()
    {
        if (HasStarted && !GameOver)
        {
            if (currentProgress >= requiredProgress)
            {
                if (!triggeredWinAnim)
                {
                    PlayerAnimator.SetTrigger ("Win");
                    triggeredWinAnim = true;
                    HeldRubble.SetActive(false);
                    PauseGame();
                }
                else
                {
                    float animPerc = PlayerAnimator.GetCurrentAnimatorStateInfo (0).normalizedTime;
                    bool isWinAnim = PlayerAnimator.GetCurrentAnimatorStateInfo (0).IsName ("Victory_Idle");

                    if (isWinAnim && animPerc > 2)
                    {
                        Success ();
                    }
                }
            }
            else if (currentProgress <= 0)
                Fail ();

            currentProgress -= progressReductionPerSecond * Time.deltaTime;
            progressBar.fillAmount = currentProgress / requiredProgress;

        }
    }
    protected override void Update()
    {
        base.Update();
        float animPerc = PlayerAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        bool isWinAnim = PlayerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Victory_Idle");

        if (isWinAnim && animPerc > 2 && !won)
        {
            Success();
            won = true;
        }

    }
    public override void Reset ()
    {
        base.Reset ();
    }

    public override void ResetUI ()
    {
        base.ResetUI ();
        progressBar.fillAmount = 0;
        progressBar.transform.parent.gameObject.SetActive (true);
    }

    protected override void Success ()
    {
        base.Success ();

    }

    protected override void Fail ()
    {
        base.Fail ();

    }
}