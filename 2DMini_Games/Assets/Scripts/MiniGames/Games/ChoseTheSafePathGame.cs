﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoseTheSafePathGame : MiniGameBase
{
    public List<Sprite> CorImage, IncorImage;
    public int Rounds,InputChoice;
    bool PickedRight, PickedWrong;
    public List<GameObject> Choices;
    public GameObject canvas;
    public GameObject CorChoice, IncChoice;
    float xpos;
   
    public override void OnEnable()
    {
        base.OnEnable();
        //base.MiniGameCamera.transform.position = new Vector3(0,0,0);
        var currentDiffData = currentDifficultyData as ChoseTheSafePathDificulty;
        canvas.GetComponent<Canvas>();
        Rounds = currentDiffData.Rounds;
        CreatChoices();
        PickedRight = false;
        PickedWrong = false;
    }
    public void ClickedCorrect()
    {
        PickedRight = true;
    }
    public void ClickedIncorrect()
    {
        PickedWrong = true;
    }
    public override void OnDisable()
    {
        base.OnDisable();
        if (Choices.Count > 0)
        {
            for (int i = 0; i < 2; i++)
            {
                Destroy(Choices[i]);
            }
           
                Choices.Clear();
           
        }
    }
    public override void ControlsUpdate()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (Choices[InputChoice].tag == "Correct")
            {
                PickedRight = true;
            }
            else { PickedWrong = true; }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            InputChoice=0;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            InputChoice=1;
        }
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        if (PickedWrong)
        {
            Fail();
            PickedWrong = false;
        }
        if (PickedRight)
        {

            ClearImages();
            if (Rounds < 1)
            {
                base.Success();

            }
            else
            {
                CreatChoices();
                Rounds--;
                PickedRight = false;
            }
            
        }
    }
    public void CreatChoices()
    {
        int CorPos = 1;
        int Incorpos = 1;

        for (int i = 0; i < 2; i++)
        {
            if (i == 0)
            {
                xpos = canvas.transform.position.x * 0.75f;
            }
            if (i == 1)
            {
                xpos = canvas.transform.position.x * 1.25f;
            }
            if (CorPos > 0)
            {
                if (Incorpos > 0)
                {
                    if (Random.Range(1, 10) < 5)
                    {
                        SpawnImage(CorChoice, canvas, ref CorPos, xpos,CorImage);
                    }
                    else
                    {
                        SpawnImage(IncChoice, canvas, ref Incorpos, xpos, IncorImage);
                    }
                }
                else
                {
                    SpawnImage(CorChoice, canvas, ref CorPos, xpos, CorImage);
                }
            }
            else if (Incorpos > 0)
            {
                SpawnImage(IncChoice, canvas, ref Incorpos, xpos,IncorImage);
            }
        }
    }
    public void SpawnImage(GameObject choice, GameObject canvass, ref int typechoice,float Xposition, List<Sprite> Imagess)
    {
        GameObject creatImage = Instantiate(choice);
        creatImage.transform.SetParent(canvass.transform, false);
        creatImage.GetComponent<Image>().sprite = Imagess[Random.Range(0, Imagess.Count - 1)];
        creatImage.transform.position = new Vector3(Xposition, canvass.transform.position.y-12.5f, canvass.transform.position.z);
        typechoice--;
        //creatImage.transform.SetSiblingIndex(0);
        Choices.Add(creatImage);
    }
    public void ClearImages()
    {
        for (int i = 0; i < 2; i++)
        {
            Destroy(Choices[i]);
        }
        Choices.Clear();
    }
}
