﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckGame : MiniGameBase
{
    public GameObject Player;

    public Animator PlayerAnimator;
    //public Renderer palyerrender;
    public GameObject Door;
    public float CamFollowSpeed = 5f;
    public float TransitionSpeed = 7.5f;
    public int Earthquack;
    public int CurEarthQuack;
    public float earthquacktime;
    public bool isQuaking;
    public bool UnderTable;
    public float Quaking;
    public float Quakingtime;
    public float timeCanStandWhileQuaking;
    public float standingWhilequaking;

    private float originalZ;
    private bool standUpDelayFinished;
    private bool justCrouched;
    private bool isCameraShaking;
    public float playerspeed;
    // Use this for initialization

    public override void OnEnable()
    {
        base.OnEnable();
        originalZ = Player.transform.position.z;
        standUpDelayFinished = true;
        Player.transform.position = new Vector3(-50.0f, Player.transform.position.y, Player.transform.position.z);
        var currentDiffData = currentDifficultyData as DuckGameDificulty;
        timeCanStandWhileQuaking = currentDiffData.timeCanStandWhileQuaking;
        //Earthquack = currentDiffData.Earthquack;
       // Quakingtime = currentDiffData.Quakingtime;
        CurEarthQuack = Earthquack;
        playerspeed = currentDiffData.playerSpeed;
        earthquacktime = Random.Range(1, initialTimer / Earthquack);
    }

    public override void ControlsUpdate()
    {
        if (HasStarted)
        {

            if (!UnderTable)
            {
                if (standUpDelayFinished)
                {
                    Player.transform.position += new Vector3(playerspeed, 0, 0);
                }
            }
            if (Input.touchCount > 0)
            {
                Player.transform.position = Vector3.Lerp(Player.transform.position, new Vector3(Player.transform.position.x, Player.transform.position.y, 5f), Time.deltaTime * TransitionSpeed);
                UnderTable = true;
                justCrouched = true;
                PlayerAnimator.SetBool("Crouching", true);
            }
            else
            {
                if (standUpDelayFinished && justCrouched)
                {
                    justCrouched = false;
                    StartCoroutine(StandUpDelay());
                }
                Player.transform.position = Vector3.Lerp(Player.transform.position, new Vector3(Player.transform.position.x, Player.transform.position.y, originalZ), Time.deltaTime * TransitionSpeed);
                UnderTable = false;
                PlayerAnimator.SetBool("Crouching", false);
            }
        }
    }
    private void LateUpdate()
    {
        //Vector3 camPos = MiniGameCamera.transform.position;
        //camPos.x = Player.transform.position.x;

        //MiniGameCamera.transform.position = Vector3.Lerp(MiniGameCamera.transform.position, camPos, Time.deltaTime * CamFollowSpeed);

    }
    public override void Reset()
    {
        base.Reset();

    }
    public override void RulesUpdate()
    {
        if (HasStarted && !GameOver)
        {
            if (!isQuaking && Earthquack > 0)
            {
                if (earthquacktime < 0)
                {
                    isQuaking = true;
                    Earthquack--;
                    Quaking = Quakingtime;
                    earthquacktime = Random.Range(1, initialTimer / CurEarthQuack);
                }
                earthquacktime -= Time.deltaTime;

            }
            if (isQuaking)
            {
                if (!isCameraShaking)
                {
                    StartCoroutine(_ProcessShake(0.75f, Quakingtime));
                }

                Quaking -= Time.deltaTime;
                if (!UnderTable)
                {
                    standingWhilequaking += Time.deltaTime;
                }
                if (Quaking < 0)
                {
                    //palyerrender.material = def;
                    standingWhilequaking = 0;
                    Quaking = Quakingtime;
                    isQuaking = false;
                }
            }
            if (Vector3.Distance(Player.transform.position, Door.transform.position) < 4.5)
            {
                Success();
            }

            if (standingWhilequaking > timeCanStandWhileQuaking)
            {
                Fail();
            }
        }
    }

    private IEnumerator StandUpDelay()
    {
        standUpDelayFinished = false;
        yield return new WaitForSeconds(0.1f);
        standUpDelayFinished = true;
    }

    private IEnumerator _ProcessShake(float shakeIntensity, float shakeTiming)
    {
        isCameraShaking = true;
        Noise(0.25f, shakeIntensity);
        yield return new WaitForSeconds(shakeTiming);
        Noise(0, 0);
        isCameraShaking = false;
    }

    public void Noise(float amplitudeGain, float frequencyGain)
    {
        //MiniGameCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = amplitudeGain;
        //MiniGameCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = frequencyGain;

    }

}