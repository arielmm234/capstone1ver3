﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Sound
{
    public string ClipName;
    public AudioClip Audio;
    [Range(0, 1)]
    public float Volume = 1;
    [Range(0.1f, 3.0f)]
    public float Pitch = 1;

    public bool PlayOnAwake;
    public bool Loop;

    [HideInInspector]
    public AudioSource Source;

}

public class AudioManager : MonoBehaviour
{
    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<AudioManager> ();
                if (!instance)
                {
                    GameObject newInstance = new GameObject ("Audio Manager");
                    instance = newInstance.AddComponent<AudioManager> ();
                }
            }
            return instance;
        }
    }

    public List<Sound> AudioClips;

    private void Awake ()
    {
        if(this != instance && instance != null)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach(Sound s in AudioClips)
        {
            s.Source = gameObject.AddComponent<AudioSource>();
            s.Source.clip = s.Audio;

            s.Source.volume = s.Volume;
            s.Source.pitch = s.Pitch;

            s.Source.playOnAwake = s.PlayOnAwake;
            s.Source.loop = s.Loop;
        }
    }

    public void Play(string clipName)
    {
        Debug.Log(clipName);
        Sound s = AudioClips.Find(sound => sound.ClipName == clipName);
        if(s == null)
        {
            Debug.LogWarning("Audio Clip [" + clipName + "] could not be played.");
            return;
        }
        s.Source.Play();
    }

    public void Stop(string clipName)
    {
        Sound s = AudioClips.Find(sound => sound.ClipName == clipName);
        if(s == null)
        {
            Debug.LogWarning("Audio Clip [" + clipName + "] could not be stopped.");
            return;
        }
        s.Source.Stop();
    }

    public AudioSource GetSource(string clipName)
    {
        return AudioClips.Find(sound => sound.ClipName == clipName).Source;
    }
}