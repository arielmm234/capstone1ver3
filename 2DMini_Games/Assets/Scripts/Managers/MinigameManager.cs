﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MinigameManager : Singleton<MinigameManager>
{
    public GameDifficulty CurrentDifficulty;// { get; private set; }
    public GameType currentGameType { get; private set; }
    public bool NotMini;
    [SerializeField] List<int> LevelIntervals;
    public Transform InstructionParent;
    public List<MiniGameBase> Minigames;
    public List<MiniGameBase> BossMinigames;
    public List<MiniGameBase> PlayableMiniGames;
    public List<GameObject> xMarks, hearts;
    public GameObject winBackground, loseBackground, circle, Startnextgamebutton;
    public TextMeshProUGUI GameName, RoundText, Dialog;
    public GameObject Tutorial;
    public ScriptableLife Life;
    [HideInInspector]
    public MiniGameBase currMinigame;

    void Start()
    {
        AddIntervals();

        //AudioManager.Instance.Play("Tutorial Theme Song");
        GetPlayableMiniGames();
        for (int i = 0; i < Minigames.Count; i++)
        {
            Minigames[i].GameName = GameName;
            Minigames[i].Dialog = Dialog;
            Minigames[i].Startnextgamebutton = Startnextgamebutton;
            Minigames[i].InstructionParent = InstructionParent;
            Minigames[i].WinBackground = winBackground;
            Minigames[i].LoseBackground = loseBackground;
            Minigames[i].Xmarks = xMarks;
            Minigames[i].hearts = hearts;
            Minigames[i].circle = circle;
            Minigames[i].Life = Life;
            Minigames[i].RoundText = RoundText;
            Minigames[i].Tutorial = Tutorial;
           
        }
        LevelUp();
        //CurrentDifficulty = GameDifficulty.Easy;
        
    }
    public void PauseGame()
    {
        for (int i = 0; i < PlayableMiniGames.Count; i++)
        {
            PlayableMiniGames[i].IsPaused = true;
        }
        
    }
    public void UnpauseGame()
    {
        for (int i = 0; i < PlayableMiniGames.Count; i++)
        {
            PlayableMiniGames[i].UnPauseGame();
        }
    }
    public void LevelUp()
    {
        currMinigame = GetNextMinigame();
        if (CurrentDifficulty != GameDifficulty.Hard)
        {
            if (currMinigame == null && NotMini)
            {
                CurrentDifficulty++;
                for (int i = 0; i < PlayableMiniGames.Count; i++)
                {
                    PlayableMiniGames[i].Reset();
                }
                PlayableMiniGames.Clear();
                GetPlayableMiniGames();
            }
            TransitionManager.Instance.ShowNextMinigame();
        }
        else if (CurrentDifficulty == GameDifficulty.Hard)
        {
            PlayableMiniGames.Clear();
            LevelIntervals.Clear();
            AddIntervals();
            currentGameType++;
            CurrentDifficulty = GameDifficulty.Tutorial;
            GetPlayableMiniGames();
            TransitionManager.Instance.ShowNextMinigame();
        }
        if ((int)currentGameType > 1)
        {
            Debug.Log("GameOver");
        }
    }
    public MiniGameBase GetNextMinigame(bool shouldDecreaseIntervals = true)
    {
        List<MiniGameBase> avaiableMinigames = new List<MiniGameBase>();
        if (CurrentDifficulty != GameDifficulty.Hard)
        {
            if (shouldDecreaseIntervals)
            {
                LevelIntervals[(int)CurrentDifficulty] -= 1;
                if (LevelIntervals[(int)CurrentDifficulty] < 0)
                {
                    return null;
                }
            }
            foreach (MiniGameBase minigame in PlayableMiniGames)
            {
                if (!minigame.GameOver && currentGameType == minigame.difficulties[(int)CurrentDifficulty].TypeOfGame)
                {
                    if (minigame.difficulties[(int)CurrentDifficulty] != null)
                    {
                        avaiableMinigames.Add(minigame);
                    }
                }
            }
            if (avaiableMinigames.Count == 0)
            {
                return null;
            }

            int rand = (int)Random.Range(0, avaiableMinigames.Count - 1);
            return avaiableMinigames[rand];
        }
        else
        {
            foreach (MiniGameBase minigame in BossMinigames)
            {
                if (!minigame.GameOver)
                {
                    avaiableMinigames.Add(minigame);
                }
            }

            if (avaiableMinigames.Count == 0)
            {
                return null;
            }
            return avaiableMinigames.Find((x) => !x.GameOver);
        }
    }
    public void GetPlayableMiniGames()
    {
        if (CurrentDifficulty != GameDifficulty.Hard)
        {
            for (int i = 0; i < Minigames.Count; i++)
            {
                if (Minigames[i].difficulties.Count > 0)
                {
                    if (Minigames[i].difficulties[(int)CurrentDifficulty] != null)
                    {
                        if (currentGameType == Minigames[i].difficulties[(int)CurrentDifficulty].TypeOfGame)
                        {
                            PlayableMiniGames.Add(Minigames[i]);
                        }
                    }
                }
            }
        }


    }
    public void AddIntervals()
    {
        for (int i = 0; i < 3; i++)
        {
            LevelIntervals.Add(Minigames.Count);

        }
    }
    public void ReturntoMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
    }

    public void PlayMinigame()
    {
        for (int i = 0; i < Minigames.Count; i++)
        {
            Minigames[i].TutorialTime = 0;
        }
    }
    public void PlaynextMinigame()
    {
        if (NotMini)
        {
            for (int i = 0; i < Minigames.Count; i++)
            {
                Minigames[i].EndGameTime = 0;
            }
        }
        else
        {
            AudioManager.Instance.Stop("LoadingscreenWin");
            AudioManager.Instance.Stop("LoadingScreenlose");
            UnityEngine.SceneManagement.SceneManager.LoadScene("MenuScene");
        }
    }
}
