﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddItems : MonoBehaviour {
    public GameObject CTEG;
    public List<GameObject> ItemsInside;

    private void Update()
    {
        CTEG = GameObject.FindGameObjectWithTag("CTEG");
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item")
        {
          
            for (int i = 0; i < CTEG.GetComponent<ChoseTheEssentialsGame>().RightItems.Count; i++)
            {
                if (CTEG.GetComponent<ChoseTheEssentialsGame>().RightItems[i].name == other.name)
                {

                    CTEG.GetComponent<ChoseTheEssentialsGame>().PickedRight(other.name);
                    for (int y = 0; y < ItemsInside.Count; y++)
                    {
                        if (ItemsInside[y].name == other.name)
                        {
                            ItemsInside[y].SetActive(true);
                        }
                    }
                    Destroy(other.gameObject);
                }
            }
        }
        if (other.tag == "WrongItem")
        {
            for (int y = 0; y < ItemsInside.Count; y++)
            {
                if (ItemsInside[y].name == other.name)
                {
                    ItemsInside[y].SetActive(true);
                }
            }
            Destroy(other.gameObject);
            CTEG.GetComponent<ChoseTheEssentialsGame>().pickedWrong();
        }
    }
  
}
