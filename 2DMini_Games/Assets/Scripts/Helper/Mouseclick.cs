﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouseclick : MonoBehaviour
{
    public GameObject game;
    private void Update()
    {
       game = GameObject.FindGameObjectWithTag("DTFGame");
    }
    public void OnMouseDown()
    {
        if(game.name == "DouseTheFireMiniGame")
        {
            if (this.tag == "Correct")
            {
                game.GetComponent<DouseTheFireGame>().ClickedCorrect();
            }
            if (this.tag == "Incorrect")
            {
                game.GetComponent<DouseTheFireGame>().ClickedIncorrect();
            }
        }
        if(game.name == "ChoseTheSafePathMiniGame")
        {
            if (this.tag == "Correct")
            {
                game.GetComponent<ChoseTheSafePathGame>().ClickedCorrect();
            }
            if (this.tag == "Incorrect")
            {
                game.GetComponent<ChoseTheSafePathGame>().ClickedIncorrect();
            }
        }


    }
    private void OnMouseOver()
    {
        this.GetComponent<Image>().color += new Color(0, 1, 0);
    }
    private void OnMouseExit()
    {
        this.GetComponent<Image>().color += new Color(0, -1, 0);
    }
   

}
