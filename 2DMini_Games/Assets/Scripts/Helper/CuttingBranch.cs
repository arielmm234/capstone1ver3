﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingBranch : MonoBehaviour {

    public TrimBranchesMiniGame game;
    public float branchCutting;
    bool isCutting;
    private void Start()
    {
       
    }
    private void Update()
    {
        if (isCutting)
        {
            branchCutting -= Time.deltaTime;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Branch")
        {
            game.IsCutting();
            isCutting = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Branch")
        {
            if (branchCutting < 0)
            {
                game.OnBranches.Remove(other.gameObject);
                game.OffBranches.Add(other.gameObject);
                other.gameObject.SetActive(false);
                isCutting = false;
                game.IsCut();
                branchCutting = 1;
            }
        }
    }
}
