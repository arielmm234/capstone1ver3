﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] CharacterController CharController;
    [SerializeField] Joystick MovementJoystick;
    [SerializeField] float Speed;
    public float CenterValue = 0;
    public bool Win;
    public bool Lose;
    bool chooseDirection;
    bool spinToDirection;
    bool isXCenter;
    float rotationAngle;
    [SerializeField] float spinTime = 1;
    float spinTimeCopy;
    GameObject turnTrigger;
    // Use this for initialization
    void OnEnable()
    {
        if (CharController == null)
            CharController = GetComponent<CharacterController>();
        spinTimeCopy = spinTime;
        MovementJoystick.gameObject.SetActive(true);


    }
    private void OnDisable()
    {
        MovementJoystick.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        WaitForInput();
        Rotate();
        if (spinToDirection || Win || Lose)
            return;
        //for debuggig
        Controls();
    }

    public void LateUpdate()
    {

        //if (Input.GetKey(KeyCode.A) || MovementJoystick.Horizontal < 0)
        //{

        //    GoLeft();
        //    // direction += new Vector3(-5.0f, 0f, 0f);
        //}
        //else if (Input.GetKey(KeyCode.D) || MovementJoystick.Horizontal > 0)
        //{
        //    GoRight();
        //    //direction += new Vector3(5.0f, 0f, 0f);
        //}
        //else
        //{
        //    //GoToCenter ();

        //}
    }

    public void Controls()
    {

        Vector3 direction = Vector3.forward * Speed;
        if (Input.GetKey(KeyCode.A) || MovementJoystick.Horizontal < 0)
        {
            direction = Vector3.forward * Speed + Vector3.left * 10;
        }
        else if (Input.GetKey(KeyCode.D) || MovementJoystick.Horizontal > 0)
        {
            direction = Vector3.forward * Speed + Vector3.right * 10;
        }
        direction = transform.TransformDirection(direction);
        CharController.SimpleMove(direction);
        if (Input.GetMouseButtonDown(0) || TouchInput.OnTouchDown())
        {
            AddSpeed(1.5f);
        }
        else
        {
            ReduceSpeed();
        }

    }

    public void GoToCenter()
    {
        if (chooseDirection || spinToDirection)
            return;

        if (isXCenter)
            transform.localPosition = Vector3.Lerp(transform.position, new Vector3(CenterValue, transform.localPosition.y, transform.localPosition.z), 2 * Time.deltaTime);
        else
            transform.localPosition = Vector3.Lerp(transform.position, new Vector3(transform.localPosition.x, transform.localPosition.y, CenterValue), 2 * Time.deltaTime);
    }
    public void GoLeft()
    {

        Vector3 direction = Vector3.left;
        direction = transform.TransformDirection (direction);
        CharController.SimpleMove (direction * 5);
     
        //if (sideValue >= 0)
        //{
        //    sideValue = transform.localPosition.x - 5.0f;
        //}

        // transform.position = Vector3.Lerp(transform.position, new Vector3(sideValue, transform.position.y, transform.position.z), 2f * Time.deltaTime);

    }

    public void GoRight()
    {
        Vector3 direction = Vector3.right;

        direction = transform.TransformDirection(direction);
        CharController.SimpleMove(direction * 8);
        //if (sideValue <= 0)
        //{
        //    sideValue = transform.localPosition.x + 5.0f;
        //}

        // transform.position = Vector3.Lerp(transform.position, new Vector3(sideValue, transform.position.y, transform.position.z), 2f * Time.deltaTime);

    }

    public void LimitSpeed()
    {
        Speed = Mathf.Clamp(Speed, 3, 10);

    }

    public void SetSpeed(float amount)
    {
        Speed = amount;
        LimitSpeed();
    }

    public void AddSpeed(float amount)
    {
        Speed += amount;
        LimitSpeed();
    }

    public void ReduceSpeed()
    {
        Speed -= Time.deltaTime;
        LimitSpeed();
    }

    public void WaitForInput()
    {
        if (chooseDirection)
        {
            if (Input.GetKey(KeyCode.A) || MovementJoystick.Horizontal < 0)
            {
                rotationAngle = -90;
                chooseDirection = false;
                spinToDirection = true;
                if (turnTrigger.transform.childCount >= 2)
                    turnTrigger.transform.GetChild(1).gameObject.SetActive(true);
            }
            else if (Input.GetKey(KeyCode.D) || MovementJoystick.Horizontal > 0)
            {
                rotationAngle = 90;
                chooseDirection = false;
                spinToDirection = true;
                if (turnTrigger.transform.childCount >= 2)
                    turnTrigger.transform.GetChild(1).gameObject.SetActive(true);
            }
        }
    }

    public void Rotate()
    {
        if (spinToDirection)
        {
            // transform.Rotate(Vector3.up, rotationAngle);
            //   spinToDirection = false;
            //     Stop = false;
            if (spinTimeCopy >= 0)
            {
                transform.Rotate(Vector3.up, rotationAngle * (Time.deltaTime / spinTime));
                spinTimeCopy -= Time.deltaTime;

            }
            else
            {
                spinTimeCopy = spinTime;
                spinToDirection = false;

            }
           
        }
    }

    public void ReachedRoutePoint(float centerValue, bool xAxis, GameObject TurnTrigger)
    {

        chooseDirection = true;
        CenterValue = centerValue;
        turnTrigger = TurnTrigger;
        isXCenter = xAxis;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WinTrigger"))
        {
            MovementJoystick.gameObject.SetActive(false);
            Win = true;
        }
        if (other.CompareTag("LoseTrigger"))
        {
            MovementJoystick.gameObject.SetActive(false);
            Lose = true;
        }
        if (other.CompareTag("Obstacle"))
        {
            SetSpeed(0);
        }
        if (other.CompareTag("TurnZTrigger"))
        {
            ReachedRoutePoint(other.gameObject.transform.position.z, false, other.gameObject);
            other.GetComponent<Collider>().enabled = false;
            other.transform.GetChild(0).gameObject.SetActive(true);
        }
        if (other.CompareTag("TurnXTrigger"))
        {
            ReachedRoutePoint(other.gameObject.transform.position.x, true, other.gameObject);
            other.GetComponent<Collider>().enabled = false;
            other.transform.GetChild(0).gameObject.SetActive(true);
        }

    }
}