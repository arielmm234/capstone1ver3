﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
[System.Serializable]
public class TutorialInfo
{
    public List<Sprite> TutorialImages = new List<Sprite>();
    public int ImageCount;
    public bool IsDone;
}
public class TutorialManager : Singleton<TutorialManager>
{
    public bool IsCurrentTutorialDone;
    [SerializeField] Image TutorialImage;
    [SerializeField] List<TutorialInfo> TutorialInfos = new List<TutorialInfo>();
    TutorialInfo currentTutorialInfo;
    public GameObject LoadingBg;


    [Space(20)]
    [Header("JOYSTICK TUT VARIABLES")]
    public GameObject MovementController;
    public GameObject CameraController;
    public GameObject ArrowPointer;

    [Space(20)]
    [Header("DRAG TUT VARIABLES")]
    public GameObject Player;
    public GameObject DragCamera;

    [Space(20)]
    [Header("TAP TUT VARIABLES")]
    public List<GameObject> ObjectsToInteract;
    //event UnityAction currentTutorialFunc;

    // Use this for initialization
    void Start()
    {
        AudioManager.Instance.Play("Tutorial Theme Song");
        StartTutorial();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void StartTutorial()
    {
        currentTutorialInfo = TutorialInfos[0];
        AudioManager.Instance.Play("Hey!");
        TutorialImage.gameObject.SetActive(true);
        GetNextTutorialImage();
    }

    public void GetNextTutorialImage()
    {
        if (currentTutorialInfo.ImageCount != currentTutorialInfo.TutorialImages.Count)
        {
            if (currentTutorialInfo.ImageCount - 1 >= 0)
            {

            }

            TutorialImage.sprite = currentTutorialInfo.TutorialImages[currentTutorialInfo.ImageCount];
            currentTutorialInfo.ImageCount++;
        }
        else
        {
            TutorialImage.gameObject.SetActive(false);
            int index = TutorialInfos.FindIndex((x) => x == currentTutorialInfo);
            if (index == 0)
                StartCoroutine(JoystickTut());
            else if (index == 1)
            {
                StartCoroutine(DragTut());
            }
            else if (index == 2)
            {
                StartCoroutine(TapTut());
            }
            else
            {
                TutorialDone();
            }
            //Do ienumerator function
        }
    }


    void GetNextTutorial()
    {
        IsCurrentTutorialDone = false;
        currentTutorialInfo.IsDone = true;
        TutorialInfo nextTutorial = TutorialInfos.Find((x) => !x.IsDone);
        if (nextTutorial != null)
        {
            currentTutorialInfo = nextTutorial;
            TutorialImage.gameObject.SetActive(true);
            GetNextTutorialImage();
        }
    }

    IEnumerator JoystickTut()
    {
        MovementController.SetActive(true);
        CameraController.SetActive(true);
        ArrowPointer.SetActive(true);
        yield return new WaitUntil(() => IsCurrentTutorialDone);
        MovementController.SetActive(false);
        CameraController.SetActive(false);
        ArrowPointer.SetActive(false);
        GetNextTutorial();
    }

    IEnumerator DragTut()
    {
        Player.SetActive(false);
        DragCamera.SetActive(true);
        DragCamera.GetComponent<TutorialInteract>().IsDragTutorial = true;
        yield return new WaitUntil(() => IsCurrentTutorialDone);
        GetNextTutorial();
    }

    IEnumerator TapTut()
    {
       
        DragCamera.GetComponent<TutorialInteract>().IsTapTutorial = true;
        yield return new WaitUntil(() => IsCurrentTutorialDone);
        GetNextTutorial();
    }

    void TutorialDone()
    {
        AudioManager.Instance.Play("Tutorial Done");
        Debug.Log("Tutorial Done");
        LoadingBg.SetActive(true);
        StartCoroutine(Delay(AudioManager.Instance.GetSource("Tutorial Done").clip.length));
    }


    IEnumerator Delay(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("GameScene");
        AudioManager.Instance.Stop("Tutorial Theme Song");
    }
}
