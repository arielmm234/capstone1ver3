﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TouchInput
{
    private static SwipeData _swipeData = new SwipeData ();

    public class TouchData
    {
        public int TouchId;
        public Vector2 TouchPosition;
        public Vector2 RawPosition;
        public Vector2 ViewportPosition;
        public TouchPhase Phase;
    }
    public enum SwipePhase
    {
        Null,
        Start,
        Swiping,
        Stationary,
        Finished
    }

    public class SwipeData
    {

        public TouchData Touch;
        public SwipePhase Phase = SwipePhase.Null;
        public Vector2 StartPosition;
        public Vector2 EndPosition;

        public Vector2 Direction
        {
            get
            {
                if (Phase == SwipePhase.Finished)
                {
                    return (EndPosition - StartPosition).normalized;
                }
                else if (Phase == SwipePhase.Swiping)
                {
                    return (Touch.TouchPosition - StartPosition).normalized;
                }
                else
                {
                    return Vector2.zero;
                }
            }
        }

        public float Magnitude
        {
            get
            {
                if (Phase == SwipePhase.Swiping)
                {
                    return (Touch.TouchPosition - StartPosition).magnitude;
                }
                else if (Phase == SwipePhase.Finished)
                {
                    return (StartPosition - EndPosition).magnitude;
                }
                else
                {
                    return 0;
                }
            }
        }

        public void ClearSwipeData ()
        {
            Touch = null;
            Phase = SwipePhase.Start;
            StartPosition = EndPosition = Vector2.zero;
        }
    }

    public static bool OnTouchDown (int index = 0)
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[index].phase == TouchPhase.Began)
            {
                return true;
            }
        }
        return false;
    }

    public static bool OnTouchUp (int index = 0)
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[index].phase == TouchPhase.Ended)
            {
                return true;
            }
        }
        return false;
    }

    public static SwipeData OnSwipe (int index = 0)
    {
        if (_swipeData.Phase == SwipePhase.Finished)
        {
            _swipeData.Phase = SwipePhase.Null;
        }

        TouchData touch = GetTouch (index);

        if (touch != null)
        {
            if (_swipeData.Touch != null)
            {
                if (touch.TouchId != _swipeData.Touch.TouchId &&
                    _swipeData.Phase == SwipePhase.Finished)
                {
                    _swipeData.ClearSwipeData ();
                }
            }

            else if (_swipeData.Touch == null)
            {
                if (_swipeData.Phase == SwipePhase.Null)
                {
                    _swipeData.Touch = touch;
                    _swipeData.Phase = SwipePhase.Start;
                }
            }
        }

        _swipeData.Touch = touch;

        if (_swipeData.Touch != null)
        {

            if (_swipeData.Touch.Phase == TouchPhase.Began)
            {
                _swipeData.StartPosition = _swipeData.Touch.TouchPosition;
                _swipeData.Phase = SwipePhase.Start;
            }
            if (_swipeData.Touch.Phase == TouchPhase.Moved)
            {
                _swipeData.EndPosition = _swipeData.Touch.TouchPosition;
                _swipeData.Phase = SwipePhase.Swiping;
            }
            if (_swipeData.Touch.Phase == TouchPhase.Ended)
            {
                _swipeData.EndPosition = _swipeData.Touch.TouchPosition;
                _swipeData.Phase = SwipePhase.Finished;
            }
            if (_swipeData.Touch.Phase == TouchPhase.Stationary)
            {
                _swipeData.Phase = SwipePhase.Stationary;
            }

        }

        return _swipeData;
    }

    public static TouchData GetTouch (int touchIndex)
    {
        if (Input.touchCount > 0 && touchIndex < Input.touchCount)
        {
            Touch touch = Input.touches[touchIndex];
            TouchData data = new TouchData ()
            {
                TouchId = touch.fingerId,
                TouchPosition = touch.position,
                RawPosition = touch.rawPosition,
                Phase = touch.phase,
            };

            data.ViewportPosition = Camera.main.ScreenToViewportPoint (touch.rawPosition);

            return data;
        }
        //        Debug.Log ("The \"touchIndex\" is out of range");
        return null;
    }
}