﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMousedrag : MonoBehaviour {
    private void Start()
    {
        this.GetComponent<Animator>().SetTrigger("Panic Running");
    }
    private void OnMouseDrag()
    {

        this.GetComponent<Animator>().SetTrigger("Idle");
        Vector3 mousePosiion = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 13.8f);
        Vector3 objectpos = Camera.main.ScreenToWorldPoint(mousePosiion);
        transform.position = objectpos;
    }
  

}
