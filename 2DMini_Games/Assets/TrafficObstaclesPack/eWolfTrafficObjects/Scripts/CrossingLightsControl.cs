﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace eWolfTrafficObjects
{
    public class CrossingLightsControl : MonoBehaviour
    {
        /// <summary>
        /// The order of lights
        /// </summary>
        public enum CrossingLightSequences
        {
            Red,
            Green,
            GreenFlashing,
            Loop
        }

        /// <summary>
        /// Whether or not this is a slaved controller
        /// </summary>
        public bool Slave = false;

        /// <summary>
        /// The starting light mode
        /// </summary>
        public CrossingLightSequences Light = CrossingLightSequences.Red;

        /// <summary>
        /// The time the lights will stay on red
        /// </summary>
        public int OnRed = 500;

        /// <summary>
        /// The time the lights will stay on green
        /// </summary>
        public int OnGreen = 500;

        /// <summary>
        /// The time the lights will stay on green flashing
        /// </summary>
        public int OnGreenFlashing = 200;

        /// <summary>
        /// Extra lights to control
        /// </summary>
        public TrifficLightArray LightArray = new TrifficLightArray();

        /// <summary>
        /// Gets and Sets the light color
        /// </summary>
        public CrossingLightSequences LightColor
        {
            get
            {
                return Light;
            }
            set
            {
                Light = value;
                SetLights();
                SetDelayForSequence();

                if (!Slave)
                    LightArray.SyncedLights(Convert(Light));
            }
        }

        /// <summary>
        /// The main update
        /// </summary>
        public void Update()
        {
            if (_lightsList == null)
            {
                LightColor = LightColor;
            }

            if (Light == CrossingLightSequences.GreenFlashing)
            {
                bool flash = (_delay / 5 % 2) == 0;
                _lightsList[(int)CrossingLightColorIndex.Green].SetActive(flash);
            }

            if (_delay-- == 0)
            {
                if (!Slave)
                {
                    LightColor += 1;
                }
            }
        }

        /// <summary>
        /// Called when selected
        /// </summary>
        public void OnDrawGizmosSelected()
        {
            if (!Slave)
                LightArray.Draw(transform.position);
        }

        private TrifficLightControl.LightSequences Convert(CrossingLightSequences seq)
        {
            switch (Light)
            {
                case CrossingLightSequences.Red:
                    {
                        return TrifficLightControl.LightSequences.Green;
                    }
                case CrossingLightSequences.Green:
                    {
                        return TrifficLightControl.LightSequences.Red;
                    }
                case CrossingLightSequences.GreenFlashing:
                    {
                        return TrifficLightControl.LightSequences.Amber;
                    }
            }
            return TrifficLightControl.LightSequences.Red;
        }

        /// <summary>
        /// Set the light to visiable by hiding the black out objects
        /// </summary>
        private void SetLights()
        {
            if (_lightsList == null)
            {
                PopulateLights();
            }

            if (Light >= CrossingLightSequences.Loop)
                Light = CrossingLightSequences.Red;

            switch (Light)
            {
                case CrossingLightSequences.Red:
                    {
                        _lightsList[(int)CrossingLightColorIndex.Red].SetActive(true);
                        _lightsList[(int)CrossingLightColorIndex.Green].SetActive(false);
                    }
                    break;

                case CrossingLightSequences.Green:
                    {
                        _lightsList[(int)CrossingLightColorIndex.Red].SetActive(false);
                        _lightsList[(int)CrossingLightColorIndex.Green].SetActive(true);
                    }
                    break;

                case CrossingLightSequences.GreenFlashing:
                    {
                        _lightsList[(int)CrossingLightColorIndex.Red].SetActive(false);
                        _lightsList[(int)CrossingLightColorIndex.Green].SetActive(true);
                    }
                    break;
            }
        }

        /// <summary>
        /// Set the delay for each part of the sequence
        /// </summary>
        private void SetDelayForSequence()
        {
            switch (Light)
            {
                case CrossingLightSequences.Red:
                    {
                        _delay = OnRed;
                    }
                    break;

                case CrossingLightSequences.Green:
                    {
                        _delay = OnGreen;
                    }
                    break;

                case CrossingLightSequences.GreenFlashing:
                    {
                        _delay = OnGreenFlashing;
                    }
                    break;
            }
        }

        /// <summary>
        /// Populate the list of lights (once)
        /// </summary>
        private void PopulateLights()
        {
            GameObject[] lights = new GameObject[3];

            Transform[] allChildren = GetComponentsInChildren<Transform>(true);
            foreach (Transform child in allChildren)
            {
                if (child.name == "CrossingLight-Green")
                {
                    lights[(int)CrossingLightColorIndex.Green] = child.gameObject;
                }
                if (child.name == "CrossingLight-Red")
                {
                    lights[(int)CrossingLightColorIndex.Red] = child.gameObject;
                }
            }

            _lightsList = lights.ToList();
            if (!Slave)
                LightArray.SetToSlaved();
        }

        private enum CrossingLightColorIndex
        {
            Red,
            Green
        };

        private int _delay = 0;
        private List<GameObject> _lightsList = null;
    }
}