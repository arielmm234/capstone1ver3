﻿using eWolfTrafficObjects;
using UnityEngine;

public class ShowGetLights : MonoBehaviour
{
    public GameObject Light;
    public GameObject LightToSet;

    private void OnGUI()
    {
        if (Light != null)
        {
            TrifficLightControl tlc = Light.GetComponent<TrifficLightControl>();
            if (tlc != null)
            {
                GUI.Label(new Rect(10, 10, 200, 50), "Light is on " + tlc.LightColor);
            }
        }

        if (GUI.Button(new Rect(320, 10, 100, 50), "Set Red"))
        {
            SetLight(LightToSet, TrifficLightControl.LightSequences.Red);
        }

        if (GUI.Button(new Rect(320, 60, 100, 50), "Set Green"))
        {
            SetLight(LightToSet, TrifficLightControl.LightSequences.Green);
        }
    }

    private void SetLight(GameObject lightToSet, TrifficLightControl.LightSequences lightColour)
    {
        TrifficLightControl tlc = lightToSet.GetComponent<TrifficLightControl>();
        if (tlc != null)
        {
            tlc.LightColor = lightColour;
        }
    }
}
