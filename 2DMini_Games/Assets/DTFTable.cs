﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DTFTable : MonoBehaviour {
    public DouseTheFireGame game;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Correct")
        {
            game.ClickedCorrect();
        }
        else
        {
            game.ClickedIncorrect();
        }
    }
}
