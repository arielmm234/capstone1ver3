﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunOnStreetGame : MiniGameBase {

    [Header("Game Variables")]
    [SerializeField] Movement Player;
    public override void OnEnable()
    {
        base.OnEnable();
    }
    public override void OnDisable()
    {
        base.OnDisable();

    }
    public override void ControlsUpdate()
    {
        Player.Controls();

    }

    public override void RulesUpdate()
    {
        if (HasStarted && !GameOver)
        {
            if (Player.Win)
                Success();
            else if (Player.Lose)
                Fail();
        }
    }

    public override void Reset()
    {
        base.Reset();
    }


    protected override void Success()
    {
        base.Success();
    }

    protected override void Fail()
    {
        base.Fail();

    }
}
