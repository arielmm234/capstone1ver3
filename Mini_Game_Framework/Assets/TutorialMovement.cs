﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMovement : MonoBehaviour {
    [SerializeField] float TurnSpeed;
    [SerializeField] float MoveSpeed;
    [SerializeField] CharacterController CharController;
    [SerializeField] Joystick CameraJoystick;
    [SerializeField] Joystick MovementJoystick;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Move();
        Rotate();
     
	}

    void Rotate()
    {
        if (!CameraJoystick.isActiveAndEnabled)
            return;
        transform.Rotate(Vector3.up, CameraJoystick.Horizontal * (TurnSpeed * Time.deltaTime));
        transform.GetChild(0).transform.Rotate(Vector3.right, CameraJoystick.Vertical * (-TurnSpeed * Time.deltaTime));
    }

    void Move()
    {
        if (!MovementJoystick.isActiveAndEnabled)
            return;
        Vector3 direction = Vector3.zero;
        direction = new Vector3(MovementJoystick.Horizontal, 0, MovementJoystick.Vertical);
        direction = transform.TransformDirection(direction);
        CharController.SimpleMove(direction * MoveSpeed);
    }


     void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("TutorialTrigger"))
        {
            TutorialManager.Instance.IsCurrentTutorialDone = true;
        }
    }
}
