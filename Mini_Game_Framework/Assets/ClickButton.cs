﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickButton : MonoBehaviour {

    public GameObject game;

    // Update is called once per frame
    void Update() {
        game = GameObject.FindGameObjectWithTag("DTFGame");
        if (game.name == "DouseTheFireMiniGame")
        {
            if (this.tag == "Correct")
                this.GetComponent<Button>().onClick.AddListener(CorrectSelection);
            else
                this.GetComponent<Button>().onClick.AddListener(IncorrectSelection);
        }
        if (game.name == "ChoseTheSafePathMiniGame")
        {
            if (this.tag == "Correct")
                this.GetComponent<Button>().onClick.AddListener(CTFPCorrectSelection);
            else
                this.GetComponent<Button>().onClick.AddListener(CTFPIncorrectSelection);
        }
        if (game.name == "DontPressTheButtonMiniGame")
        {
            this.GetComponent<Button>().onClick.AddListener(DPRBCorrectSelection);
        }
    }
    public void CorrectSelection()
    {
        game.GetComponent<DouseTheFireGame>().ClickedCorrect();
    }
    public void IncorrectSelection()
    {
        game.GetComponent<DouseTheFireGame>().ClickedIncorrect();
    }
    public void CTFPCorrectSelection()
    {
        game.GetComponent<ChoseTheSafePathGame>().ClickedCorrect();
    }
    public void CTFPIncorrectSelection()
    {
        game.GetComponent<ChoseTheSafePathGame>().ClickedIncorrect();
    }
    public void DPRBCorrectSelection()
    {
        game.GetComponent<DontPressTheButtonGame>().Clicked();
    }
}
