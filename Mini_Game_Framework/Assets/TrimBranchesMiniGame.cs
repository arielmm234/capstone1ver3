﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrimBranchesMiniGame : MiniGameBase
{
    public GameObject Twizers, TreeWithBranch, treeStopPlace, BackCamera;
    public Vector3 CurPos;
    public Quaternion curRo;
    public List<GameObject> OffBranches,OnBranches;
    public float speedMod;
    public bool GoingToCut, goingDown, Cutting, Cut;
    public float rotationsPerMinute, CutterSpeed;
    public bool won;

    public override void OnEnable()
    {
        base.OnEnable();
        var currentDiffData = currentDifficultyData as TrimmTheBranchesDifficulty;
        goingDown = false;
        GoingToCut = false;
        Cutting = false;
        Cut = false;
        won = false;
        speedMod = currentDiffData.RotationSpeed;
        pickBranches(OffBranches, OnBranches, currentDiffData.NumberOfBranches);
        Twizers.transform.position = new Vector3(0.0016f, -0.024f, 0.1194f);
        Twizers.transform.rotation = Quaternion.Euler(-1.724f,90,20);
        BackCamera.transform.position = new Vector3(0,0,0);
        BackCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
        //Quaternion lookAt = Quaternion.LookRotation(transform.position - TreeWithBranch.transform.position);
        //Twizers.transform.rotation = lookAt;
        //Twizers.transform.LookAt(TreeWithBranch.transform);
    }
    public override void OnDisable()
    {
        for (int i = 0; i < OnBranches.Count; i++)
        {
            OnBranches[i].SetActive(false);
            OffBranches.Add(OnBranches[i]);
        }
        OnBranches.Clear();
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {

        if (!GoingToCut && !goingDown && !Cutting)
        {
            Twizers.transform.RotateAround(TreeWithBranch.transform.position, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
            BackCamera.transform.RotateAround(TreeWithBranch.transform.position, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * speedMod);
        }
        if (OnBranches.Count < 1 && !won)
        {
            won = true;
            Success();
        }
        if (GoingToCut)
        {
            Twizers.transform.position += (Twizers.transform.up * Time.deltaTime) / CutterSpeed;
        }
        if (goingDown && !Cutting)
        {
            Twizers.transform.position += (-Twizers.transform.up * Time.deltaTime) / CutterSpeed;
        }
        if (Vector3.Distance(treeStopPlace.transform.position, Twizers.transform.position) < 0.025f)
        {

            GoingToCut = false;
            goingDown = true;
        }
        if (Vector3.Distance(CurPos, Twizers.transform.position) <= 0.003f && goingDown )
        {
            Twizers.transform.position = new Vector3(CurPos.x,CurPos.y, CurPos.z);
            Twizers.transform.rotation = Quaternion.Euler(curRo.eulerAngles.x, curRo.eulerAngles.y, curRo.eulerAngles.z);
            goingDown = false;
        }
        if (Cutting && Cut)
        {
            //do animation
            
            Cutting = false;
            Cut = false;
        }
    }
    public override void ControlsUpdate()
    {

        if ((TouchInput.OnTouchDown() || Input.GetMouseButtonDown(0)) && !GoingToCut && !goingDown && !Cutting)
        {
            GoingToCut = true;
            CurPos = Twizers.transform.position;
            curRo = Twizers.transform.rotation;
        }

        base.ControlsUpdate();
    }
    public void IsCutting()
    {
        Twizers.GetComponent<Animator>().SetTrigger("Cutting");
        Cutting = true;
        GoingToCut = false;
    }
    
    public void IsCut()
    {
        goingDown = true;
        Cut = true;
    }
    public void pickBranches(List<GameObject> offbrances, List<GameObject> Onbrances, int numOfBranches)
    {
            for (int i = 0; i < offbrances.Count; i++)
            {
                if (Random.Range(0, 2) == 1)
                {
                    offbrances[i].SetActive(true);
                    Onbrances.Add(offbrances[i]);
                    offbrances.Remove(offbrances[i]);
                if (Onbrances.Count == numOfBranches)
                {
                    break;
                }
                }
            }
        if (Onbrances.Count < numOfBranches)
        {
            pickBranches(offbrances, Onbrances, numOfBranches);
        }
    }
}

