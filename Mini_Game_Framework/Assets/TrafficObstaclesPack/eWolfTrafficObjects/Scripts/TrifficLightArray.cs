﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace eWolfTrafficObjects
{
    [Serializable]
    public class TrifficLightArray
    {
        public List<TrifficLightControl> TrifficLightSync;
        public List<TrifficLightControl> TrifficLightSyncOpposite;
        public List<CrossingLightsControl> CrossingLightSync;
        public List<CrossingLightsControl> CrossingLightSyncOpposite;

        public static CrossingLightsControl.CrossingLightSequences GetCrossingLightColor(TrifficLightControl.LightSequences seq)
        {
            switch (seq)
            {
                case TrifficLightControl.LightSequences.Red:
                    {
                        return CrossingLightsControl.CrossingLightSequences.Green;
                    }
                case TrifficLightControl.LightSequences.RedAmber:
                    {
                        return CrossingLightsControl.CrossingLightSequences.GreenFlashing;
                    }
                case TrifficLightControl.LightSequences.Green:
                    {
                        return CrossingLightsControl.CrossingLightSequences.Red;
                    }
                case TrifficLightControl.LightSequences.Amber:
                    {
                        return CrossingLightsControl.CrossingLightSequences.Red;
                    }
            }
            return CrossingLightsControl.CrossingLightSequences.Red;
        }

        public static CrossingLightsControl.CrossingLightSequences GetCrossingLightColorOpposite(TrifficLightControl.LightSequences seq)
        {
            switch (seq)
            {
                case TrifficLightControl.LightSequences.Red:
                    {
                        return CrossingLightsControl.CrossingLightSequences.Red;
                    }
                case TrifficLightControl.LightSequences.RedAmber:
                    {
                        return CrossingLightsControl.CrossingLightSequences.Red;
                    }
                case TrifficLightControl.LightSequences.Green:
                    {
                        return CrossingLightsControl.CrossingLightSequences.Green;
                    }
                case TrifficLightControl.LightSequences.Amber:
                    {
                        return CrossingLightsControl.CrossingLightSequences.GreenFlashing;
                    }
            }
            return CrossingLightsControl.CrossingLightSequences.Red;
        }

        public void Draw(Vector3 pos)
        {
            Gizmos.color = Color.red;
            foreach (TrifficLightControl tlc in TrifficLightSync)
            {
                if (tlc != null)
                    Gizmos.DrawLine(pos, tlc.gameObject.transform.position);
            }

            Gizmos.color = Color.green;
            foreach (TrifficLightControl tlc in TrifficLightSyncOpposite)
            {
                if (tlc != null)
                    Gizmos.DrawLine(pos, tlc.gameObject.transform.position);
            }

            Gizmos.color = Color.red;
            foreach (CrossingLightsControl tlc in CrossingLightSync)
            {
                if (tlc != null)
                    Gizmos.DrawLine(pos, tlc.gameObject.transform.position);
            }

            Gizmos.color = Color.green;
            foreach (CrossingLightsControl tlc in CrossingLightSyncOpposite)
            {
                if (tlc != null)
                    Gizmos.DrawLine(pos, tlc.gameObject.transform.position);
            }
        }

        public void SetToSlaved()
        {
            foreach (TrifficLightControl tlc in TrifficLightSync)
            {
                if (tlc != null)
                    tlc.Slave = true;
            }

            foreach (TrifficLightControl tlc in TrifficLightSyncOpposite)
            {
                if (tlc != null)
                    tlc.Slave = true;
            }

            foreach (CrossingLightsControl tlc in CrossingLightSync)
            {
                if (tlc != null)
                    tlc.Slave = true;
            }

            foreach (CrossingLightsControl tlc in CrossingLightSyncOpposite)
            {
                if (tlc != null)
                    tlc.Slave = true;
            }
        }

        public void SyncedLights(TrifficLightControl.LightSequences seq)
        {
            foreach (TrifficLightControl tlc in TrifficLightSync)
            {
                if (tlc != null)
                    tlc.LightColor = seq;
            }

            TrifficLightControl.LightSequences light = TrifficLightControl.GetOpposite(seq);

            foreach (TrifficLightControl tlc in TrifficLightSyncOpposite)
            {
                if (tlc != null)
                    tlc.LightColor = light;
            }

            CrossingLightsControl.CrossingLightSequences crossingLightSequences = GetCrossingLightColor(seq);
            foreach (CrossingLightsControl tlc in CrossingLightSync)
            {
                if (tlc != null)
                    tlc.LightColor = crossingLightSequences;
            }

            CrossingLightsControl.CrossingLightSequences crossingLightSequencesOpp = GetCrossingLightColorOpposite(seq);

            foreach (CrossingLightsControl tlc in CrossingLightSyncOpposite)
            {
                if (tlc != null)
                    tlc.LightColor = crossingLightSequencesOpp;
            }
        }
    }
}