﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace eWolfTrafficObjects
{
    public class TrifficLightControl : MonoBehaviour
    {
        /// <summary>
        /// The order of lights
        /// </summary>
        public enum LightSequences
        {
            Red,
            RedAmber,
            Green,
            Amber,
            Loop
        }

        /// <summary>
        /// Whether or not this is a slaved controller
        /// </summary>
        public bool Slave = false;

        /// <summary>
        /// The starting light mode
        /// </summary>
        public LightSequences Light = LightSequences.Red;

        /// <summary>
        /// The time the lights will stay on red
        /// </summary>
        public int OnRed = 500;

        /// <summary>
        /// The time the lights will stay on red and amber
        /// </summary>
        public int OnRedAmber = 100;

        /// <summary>
        /// The time the lights will stay on green
        /// </summary>
        public int OnGreen = 500;

        /// <summary>
        /// The time the lights will stay on amber
        /// </summary>
        public int OnAmber = 100;

        /// <summary>
        /// Extra lights to control
        /// </summary>
        public TrifficLightArray LightArray = new TrifficLightArray();

        /// <summary>
        /// Gets and Sets the light color
        /// </summary>
        public LightSequences LightColor
        {
            get
            {
                return Light;
            }
            set
            {
                Light = value;
                SetLights();
                SetDelayForSequence();

                if (!Slave)
                    LightArray.SyncedLights(Light);
            }
        }

        /// <summary>
        /// The main update
        /// </summary>
        public void Update()
        {
            if (_lightsList == null)
            {
                LightColor = LightColor;
            }

            if (_delay-- == 0)
            {
                if (!Slave)
                {
                    LightColor += 1;
                }
            }
        }

        /// <summary>
        /// Called when selected
        /// </summary>
        public void OnDrawGizmosSelected()
        {
            if (!Slave)
                LightArray.Draw(transform.position);
        }

        /// <summary>
        /// The the Opposite color
        /// </summary>
        /// <param name="seq">The light to get the Opposite of</param>
        /// <returns>The Opposite color</returns>
        public static LightSequences GetOpposite(LightSequences seq)
        {
            switch (seq)
            {
                case LightSequences.Red:
                    {
                        return LightSequences.Green;
                    }
                case LightSequences.RedAmber:
                    {
                        return LightSequences.Amber;
                    }
                case LightSequences.Green:
                    {
                        return LightSequences.Red;
                    }
                case LightSequences.Amber:
                    {
                        return LightSequences.RedAmber;
                    }
            }
            return LightSequences.Red;
        }

        /// <summary>
        /// Populate the list of lights (once)
        /// </summary>
        private void PopulateLights()
        {
            GameObject[] lights = new GameObject[3];

            Transform[] allChildren = GetComponentsInChildren<Transform>(true);
            foreach (Transform child in allChildren)
            {
                if (child.name == "BlackOutGreen")
                {
                    lights[(int)LightColorIndex.Green] = child.gameObject;
                }
                if (child.name == "BlackOutRed")
                {
                    lights[(int)LightColorIndex.Red] = child.gameObject;
                }
                if (child.name == "BlackOutAmber")
                {
                    lights[(int)LightColorIndex.Amber] = child.gameObject;
                }
            }

            _lightsList = lights.ToList();
            if (!Slave)
                LightArray.SetToSlaved();
        }

        /// <summary>
        /// Set the light to visiable by hiding the black out objects
        /// </summary>
        private void SetLights()
        {
            if (_lightsList == null)
            {
                PopulateLights();
            }

            if (Light >= LightSequences.Loop)
                Light = LightSequences.Red;

            switch (Light)
            {
                case LightSequences.Red:
                    {
                        _lightsList[(int)LightColorIndex.Red].SetActive(false);
                        _lightsList[(int)LightColorIndex.Amber].SetActive(true);
                        _lightsList[(int)LightColorIndex.Green].SetActive(true);
                    }
                    break;

                case LightSequences.RedAmber:
                    {
                        _lightsList[(int)LightColorIndex.Red].SetActive(false);
                        _lightsList[(int)LightColorIndex.Amber].SetActive(false);
                        _lightsList[(int)LightColorIndex.Green].SetActive(true);
                    }
                    break;

                case LightSequences.Green:
                    {
                        _lightsList[(int)LightColorIndex.Red].SetActive(true);
                        _lightsList[(int)LightColorIndex.Amber].SetActive(true);
                        _lightsList[(int)LightColorIndex.Green].SetActive(false);
                    }
                    break;

                case LightSequences.Amber:
                    {
                        _lightsList[(int)LightColorIndex.Red].SetActive(true);
                        _lightsList[(int)LightColorIndex.Amber].SetActive(false);
                        _lightsList[(int)LightColorIndex.Green].SetActive(true);
                    }
                    break;
            }
        }

        /// <summary>
        /// Set the delay for each part of the sequence
        /// </summary>
        private void SetDelayForSequence()
        {
            switch (Light)
            {
                case LightSequences.Red:
                    {
                        _delay = OnRed;
                    }
                    break;

                case LightSequences.RedAmber:
                    {
                        _delay = OnRedAmber;
                    }
                    break;

                case LightSequences.Green:
                    {
                        _delay = OnGreen;
                    }
                    break;

                case LightSequences.Amber:
                    {
                        _delay = OnAmber;
                    }
                    break;
            }
        }

        private enum LightColorIndex
        {
            Red,
            Amber,
            Green
        };

        private int _delay = 0;
        private List<GameObject> _lightsList = null;
    }
}
