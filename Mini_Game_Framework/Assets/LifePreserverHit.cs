﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifePreserverHit : MonoBehaviour
{
    public SaveTheManGame game;

    public bool CanThrow;

    public float StrengthThrow;
    public float MaxHeightThrow;
    public float Speed;
    public int StepCount;
    public AnimationCurve ThrowCurve;
    public Vector3 ThrowDir;

    private TouchInput.SwipeData swipeData;
    private Vector3 start, end;

    [ContextMenu ("Create Y Values")]
    private void Update ()
    {
        if (CanThrow)
        {
            if (GetThrowDir ())
            {
                CalculateYValues ();
                MoveThroughPositions ();
                CanThrow = false;
                game.OnLifePreserverThrow();
            }
        }
    }

    private void MoveThroughPositions ()
    {
        List<Vector3> positions = new List<Vector3> ();
        List<float> yPositions = CalculateYValues ();

        for (int i = 0; i < StepCount; i++)
        {
            //if (swipeData.Phase == TouchInput.SwipePhase.Finished)
            {
                Vector3 pos = Vector3.Lerp (start, end, (float) i / (float) StepCount);
                pos.z = pos.y;
                pos.y = yPositions[i];
                positions.Add (pos);
            }
        }

        // Vector3 posEnd = Vector3.Lerp (start, end, 1);
        // posEnd.z = posEnd.y;
        // posEnd.y = yPositions[0];
        // positions.Add (posEnd);

        StartCoroutine (MoveThroughWaypoints (positions));
    }

    private bool GetThrowDir ()
    {
        swipeData = TouchInput.OnSwipe ();
        if (swipeData.Phase == TouchInput.SwipePhase.Finished &&
            swipeData.Magnitude >= 60f)
        {
            ThrowDir = new Vector3 (swipeData.Direction.x, 0, swipeData.Direction.y);
            start = transform.position;
            end = start + (transform.forward * StrengthThrow);
            return true;
        }
        return false;
    }

    private List<float> CalculateYValues ()
    {
        List<float> yPositions = new List<float> ();

        for (int i = 0; i < StepCount; i++)
        {
            yPositions.Add (ThrowCurve.Evaluate ((float) i / (float) StepCount) * MaxHeightThrow);
        }
        // yPositions.Add (ThrowCurve.Evaluate (1.1f) * MaxHeightThrow);
        return yPositions;
    }

    private IEnumerator MoveThroughWaypoints (List<Vector3> positions)
    {
        int currPosTarget = 0;
        bool isFinished = currPosTarget >= positions.Count;

        while (!isFinished)
        {
            if (positions[currPosTarget] == null)
                break;
            Vector3 dir = positions[currPosTarget] - transform.position;
            transform.Translate (dir.normalized * Speed * Time.deltaTime, Space.World);
            if (Vector3.Distance (transform.position, positions[currPosTarget]) <= 0.2f)
            {
                currPosTarget++;
            }
            isFinished = currPosTarget >= positions.Count;
            yield return new WaitForEndOfFrame ();
        }
        GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        Reset();
    }

    public void Reset()
    {
        transform.position = start;
        CanThrow = true;
        GetComponent<Collider>().enabled = true;
    }

    private void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Person")
        {
            gameObject.SetActive(false);
            game.Saved();
        }
    }
}