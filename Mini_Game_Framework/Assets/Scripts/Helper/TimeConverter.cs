﻿using UnityEngine;

public static class TimeConverter {

	public static int ToMinutes(int seconds)
	{
		return Mathf.RoundToInt(seconds/60);
	}

	public static int ToMinutes(float seconds)
	{
		int sec = Mathf.RoundToInt(seconds);
		return ToMinutes(sec);
	}

}
