﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class TransitionManager : Singleton<TransitionManager>
{
    MiniGameBase currentMinigame;

    public void ShowNextMinigame()
    {
        
        MiniGameBase minigame = MinigameManager.Instance.GetNextMinigame(false);
        // MinigameManager.Instance.
        if (currentMinigame != null)
        {
            currentMinigame.transform.parent.gameObject.SetActive(false);
        }
      
        if(minigame != null)
        {
            minigame.Life.round++;

            currentMinigame = null;
            currentMinigame = minigame;
            minigame.transform.parent.gameObject.SetActive(true);
            MinigameManager.Instance.currMinigame = null;
        }

    }
}
