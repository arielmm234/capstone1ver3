﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MiniGameTimer : MonoBehaviour {

	public Image TimerBar;
	public TextMeshProUGUI TimerText;


	private MiniGameBase minigameBase;

	private void Start() {
		minigameBase = GetComponent<MiniGameBase>();
	}

	private void Update() {
		if(!TimerBar)
		{
			Debug.LogWarning("The timer bar is null", this);
			return;
		}

		if(!minigameBase.IsPaused && !minigameBase.GameOver && minigameBase.HasStarted)
		{
			TimerBar.fillAmount = minigameBase.CurrentTimer / minigameBase.InitialTimer;
			TimerText.text = minigameBase.CurrentTimer.ToString("0") + "/" + minigameBase.InitialTimer;
		}
	}

}
