﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum InstructionType
{
    Joystick,
    Tap,
    Drag
}

[RequireComponent (typeof (MiniGameTimer))]
public class MiniGameBase : MonoBehaviour
{
    
    public float CurrentTimer { get { return currentTimer; } set { currentTimer = value; } }
    public float InitialTimer { get { return initialTimer; } set { initialTimer = value; } }

    [Header ("Identifiers")]
    public string MiniGameName;
    public CinemachineVirtualCamera MiniGameCamera;
    public InstructionType Instruction;
    
    [HideInInspector]
    public List<GameObject> Xmarks, hearts;
    [HideInInspector]
    public float TutorialTime,EndGameTime;
    [HideInInspector]
    public TextMeshProUGUI GameName, RoundText;
    [HideInInspector]
    public GameObject Tutorial;
    [HideInInspector]
    public GameObject WinBackground, LoseBackground, circle;
    [HideInInspector]
    public Transform InstructionParent;
    [HideInInspector]
    public ScriptableLife Life;

    [Space]
    [Header ("Game Variables")]
    [ContextMenuItem ("To Minutes", "ToMinutes")]
    public List<MinigameDifficulty> difficulties;
    protected MinigameDifficulty currentDifficultyData;
    protected float initialTimer;
    protected float currentTimer;

    [Space]
    public bool WinEnd = false;
    public bool LoseEnd = false;
    public bool HasStarted = false;
    public bool JustStarted = false;
    public bool JustEnded = false;
    public bool IsPaused = false;
    public bool GameOver = false;

    public UnityEvent OnGameOver;
    public void Awake ()
    {
        if (difficulties.Count > 0)
        {
            currentDifficultyData = difficulties[0];
        }
        else
            Debug.Log ("No difficulties set", this);
        this.gameObject.transform.parent.gameObject.SetActive (false);
    }
    public virtual void OnEnable ()
    {
        GameName.text = MiniGameName;
        RoundText.text = "ROUND: " + Life.round.ToString ();
        Tutorial.SetActive (true);
        currentDifficultyData = difficulties[(int) MinigameManager.Instance.CurrentDifficulty];
        initialTimer = currentDifficultyData.time;
        StartMiniGame ();
    }
    public virtual void OnDisable ()
    {
        ResetUI ();
    }

    [ContextMenu ("[DEBUG] Start Mini Game")]
    public virtual void StartMiniGame ()
    {
        if (!HasStarted)
        {
            RoundText.gameObject.SetActive (true);
            GameName.gameObject.SetActive (true);
            switch (Instruction)
            {
                case InstructionType.Drag:
                    InstructionParent.GetChild (0).gameObject.SetActive (true);
                    break;
                case InstructionType.Joystick:
                    InstructionParent.GetChild (1).gameObject.SetActive (true);
                    break;
                case InstructionType.Tap:
                    InstructionParent.GetChild (2).gameObject.SetActive (true);
                    break;
            }

            HasStarted = true;
            JustStarted = true;
            TutorialTime = 3;
            EndGameTime = 3;
            currentTimer = initialTimer;
            StartCoroutine (TimerStart ());
        }
    }

    public virtual void PauseGame ()
    {
        IsPaused = true;
    }
    public virtual void UnPauseGame ()
    {
        IsPaused = false;
    }

    protected virtual void Update ()
    {

        //lifeUI.text = "Life: " + Life.life.ToString();
        if (JustStarted)
        {
            PauseGame ();
            TutorialTime -= Time.deltaTime;

            if (TutorialTime <= 0)
            {
                Tutorial.SetActive (false);
                JustStarted = false;
                UnPauseGame ();
                TutorialTime = 3;
            }
        }
        if (JustEnded)
        {

            EndGameTime -= Time.deltaTime;
            if (EndGameTime <= 0)
            {
                WinBackground.SetActive (false);
                LoseBackground.SetActive (false);
                if (WinEnd)
                {
                    circle.SetActive (false);
                    GameOver = true;
                    WinEnd = false;
                    MinigameManager.Instance.LevelUp ();
                }
                if (LoseEnd)
                {
                    Xmarks[3 - Life.life].SetActive (false);
                    hearts[3 - Life.life].SetActive (false);
                    Life.life--;
                    LoseEnd = false;
                    if (Life.life < 1)
                    {
                        UnityEngine.SceneManagement.SceneManager.LoadScene ("MenuScene");
                    }
                    else
                    {
                        GameOver = true;
                        MinigameManager.Instance.LevelUp ();
                    }
                }
                EndGameTime = 3;

                UnPauseGame ();
            }
        }
        if (!IsPaused)
        {
            ControlsUpdate ();
            RulesUpdate ();
        }
    }
    public virtual void ControlsUpdate ()
    {

    }
    public virtual void RulesUpdate ()
    {

    }
    
    protected virtual void Success ()
    {
        StartCoroutine(WaitToEnd());
        InstructionParent.GetChild(0).gameObject.SetActive(false);
        InstructionParent.GetChild(1).gameObject.SetActive(false);
        InstructionParent.GetChild(2).gameObject.SetActive(false);
        JustEnded = true;
        WinEnd = true;
        RoundText.gameObject.SetActive (false);
        GameName.gameObject.SetActive (false);
        circle.SetActive (true);
        WinBackground.SetActive (true);
        PauseGame ();
    }
    protected virtual void Fail ()
    {
        StartCoroutine(WaitToEnd());
        InstructionParent.GetChild(0).gameObject.SetActive(false);
        InstructionParent.GetChild(1).gameObject.SetActive(false);
        InstructionParent.GetChild(2).gameObject.SetActive(false);
        JustEnded = true;
        LoseEnd = true;
        RoundText.gameObject.SetActive (false);
        GameName.gameObject.SetActive (false);
        Xmarks[3 - Life.life].SetActive (true);
        WinBackground.SetActive (true);
        LoseBackground.SetActive (true);
        PauseGame ();
    }

    public virtual void Reset ()
    {
        GameOver = false;
        HasStarted = false;
        JustEnded = false;
        WinEnd = false;
        LoseEnd = false;
        IsPaused = false;

    }
    public MinigameDifficulty GetGameType ()
    {
        return currentDifficultyData;
    }

    public virtual void ResetUI ()
    {
        initialTimer = 0;
    }
    public IEnumerator WaitToEnd()
    {
        yield return new WaitForSecondsRealtime(4);
        Debug.Log("time Start");
    }
    IEnumerator TimerStart ()
    {
        while (currentTimer > 0)
        {
            if (!IsPaused && HasStarted && !GameOver)
            {
                currentTimer -= Time.deltaTime;
                Mathf.Round (currentTimer);
            }
            yield return new WaitForEndOfFrame ();
        }
        Fail ();
        OnGameOver.Invoke ();
    }
    

    private void ToMinutes ()
    {
        Debug.Log (TimeConverter.ToMinutes (initialTimer));
    }

}