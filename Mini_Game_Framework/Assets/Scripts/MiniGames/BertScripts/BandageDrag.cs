﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BandageDrag : MonoBehaviour
{
    Rigidbody rigidBody;
    [HideInInspector]
    public bool EnableMovement;
    public float ZedOffset;
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.isKinematic = true;
        EnableMovement = false;
    }
    private void OnMouseDown()
    {
        Vector3 mousePosiion = new Vector3(Input.mousePosition.x, Input.mousePosition.y, ZedOffset);
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(mousePosiion);
        newPosition.z = ZedOffset;
        EnableMovement = true;
    }
    private void OnMouseUp()
    {
        EnableMovement = false;
    }
    private void Update()
    {
        if (EnableMovement)
        {
            Vector3 mousePosiion = new Vector3(Input.mousePosition.x, Input.mousePosition.y, ZedOffset);
            Vector3 newPosition = Camera.main.ScreenToWorldPoint(mousePosiion);
            newPosition.z = ZedOffset;
            rigidBody.MovePosition(newPosition);

        }
    }
}
