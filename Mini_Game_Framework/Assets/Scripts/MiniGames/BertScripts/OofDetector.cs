﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OofDetector : MonoBehaviour
{
    public Text VelocityText;
    public Rigidbody BandageDragger;
    public BandageGame PointManager;
    [Range(0, 150)]
    public float OofThreshold;
    AudioSource source;

    IEnumerator Start()
    {
        source = GetComponent<AudioSource>();
        source.loop = false;
        yield return new WaitUntil( () => PointManager.Points.Count <= 0 );
        EndGame();
        //Destroy(BandageDragger.gameObject);
        //Destroy(VelocityText.gameObject);
        //Destroy(this.gameObject);

    }
    void Update()
    {
        if (BandageDragger)
        {
            VelocityText.text = "Velocity: " + BandageDragger.velocity.magnitude.ToString();
            if (BandageDragger.velocity.magnitude >= OofThreshold)
            {
                
                source.Play();
                EndGame();
                //Destroy(VelocityText.gameObject);
                //Destroy(BandageDragger.gameObject);
                PointManager.TooFast();

            }
        }
    }

    public void ResetGame()
    {
        BandageDragger.gameObject.SetActive(true);
    }

    public void EndGame()
    {
        BandageDragger.gameObject.SetActive(false);
    }

}
