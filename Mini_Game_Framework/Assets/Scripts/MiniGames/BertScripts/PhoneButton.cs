﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneButton : MonoBehaviour
{
    public int ButtonCharacter;
    [HideInInspector]
    public Dial911Game server;
    public Animator CaseboxAnimator;
    bool isClosedAnimation,callsedOnce;
    float animationPerc;
    
    private void Start()
    {
        callsedOnce = false;
        CaseboxAnimator = this.GetComponent<Animator>();
    }
    private void Update()
    {
        animationPerc = CaseboxAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        isClosedAnimation = CaseboxAnimator.GetCurrentAnimatorStateInfo(0).IsName("PressedButton");
      
        if (isClosedAnimation && animationPerc >= .98f && callsedOnce)
        {
            callsedOnce = false;
            server.CompareNumber(ButtonCharacter);
        }
    }
    private void OnMouseDown()
    {
        if (!callsedOnce)
        {
            callsedOnce = true;
            this.GetComponent<Animator>().SetTrigger("PressedButton");
        }
    }
}
