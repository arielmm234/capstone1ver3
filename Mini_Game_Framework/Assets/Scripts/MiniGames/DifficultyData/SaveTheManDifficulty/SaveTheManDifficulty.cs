﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SaveTheManDifficulty : MinigameDifficulty {

	public float SideMovementSpeed;
}
