﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CarryDebrisDiffcultyBase : MinigameDifficulty
{ 
    public float RequiredProgress;
    public float ProgressReductionPerSecond;
    public float ProgressPerClick;
}
