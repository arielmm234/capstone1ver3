﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class ChoseTheEssentialsDificulty : MinigameDifficulty
{
    public int CorrectItems;
    public int IncorrectItems;

	
}
