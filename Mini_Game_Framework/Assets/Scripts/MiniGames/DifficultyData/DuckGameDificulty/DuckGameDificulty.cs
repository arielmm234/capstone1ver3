﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class DuckGameDificulty : MinigameDifficulty
{

    public float timeCanStandWhileQuaking;
    public float Quakingtime;
    public int Earthquack;
    public float playerSpeed;
}
