﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class ScriptableLife : ScriptableObject {
    public int life;
    public int round;
	
}
