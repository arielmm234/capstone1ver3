﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarageTrigger : MonoBehaviour
{

	public bool CarIsInGarage;

	private void OnTriggerEnter (Collider other)
	{
		if(other.CompareTag("PlayerCar"))
		{
			if(!CarIsInGarage)
			{
				CarIsInGarage = true;
			}
		}
	}

}