﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkYourCarGame : MiniGameBase {

	public GameObject GarageDoor;
	public GarageTrigger GarageTrigger;
    // Use this for initialization
    public override void OnEnable()
    {
        base.OnEnable();
    }
    public override void OnDisable ()
    {
        base.OnDisable();
    }

    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
       
    }
    // Update is called once per frame
    protected override void Update () {
        base.Update();
        float animationPerc = GarageDoor.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime;
        bool isClosedAnimation = GarageDoor.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GarageDoor_Closed");
        if (GarageTrigger.CarIsInGarage)
		{
			GarageDoor.GetComponent<Animator>().SetTrigger("Close");
		}
        if (isClosedAnimation && animationPerc >= 1)
        {
            Success();
        }
    }
}
