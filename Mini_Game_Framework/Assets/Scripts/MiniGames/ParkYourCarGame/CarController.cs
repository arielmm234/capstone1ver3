﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AxleInfo
{
	public WheelCollider LeftWheel;
	public WheelCollider RightWheel;
	public bool Motor;
	public bool Steering;
}

public class CarController : MonoBehaviour
{
	public List<AxleInfo> AxleInfos;
	
	public FixedJoystick Motor;
	public FixedJoystick Steering;

	public float MaxMotorTorque;
	public float MaxSteeringAngle;

	private Rigidbody rb;

	void Start()
	{
		rb = GetComponent<Rigidbody>();		
	}

	private void FixedUpdate ()
	{
		float input = Input.GetAxis ("Vertical");

		float motor = MaxMotorTorque * Motor.Vertical;
		float steering = MaxSteeringAngle * Steering.Horizontal;
		foreach (AxleInfo axleInfo in AxleInfos)
		{
			if (axleInfo.Steering)
			{
				axleInfo.LeftWheel.steerAngle = steering;
				axleInfo.RightWheel.steerAngle = steering;
			}
			else if (axleInfo.Motor)
			{
				axleInfo.LeftWheel.motorTorque = motor;
				axleInfo.RightWheel.motorTorque = motor;
			}

			ApplyLocalPositionToVisualWheels (axleInfo.LeftWheel);
			ApplyLocalPositionToVisualWheels (axleInfo.RightWheel);
		}

	}

	public void ApplyLocalPositionToVisualWheels (WheelCollider collider)
	{
		if (collider.transform.childCount == 0)
		{
			return;
		}

		Transform visualWheel = collider.transform.GetChild (0);

		Vector3 position;
		Quaternion rotation;
		collider.GetWorldPose (out position, out rotation);

		visualWheel.position = position;
		visualWheel.rotation = rotation;
	}
}