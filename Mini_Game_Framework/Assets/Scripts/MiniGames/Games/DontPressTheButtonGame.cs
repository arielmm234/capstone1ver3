﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontPressTheButtonGame : MiniGameBase {

    bool PickedRight;
    public GameObject elevator;
    public bool won;
    public override void OnEnable()
    {
        base.OnEnable();
        //var currentDiffData = currentDifficultyData as ChoseTheSafePathDificulty;
        elevator.SetActive(true);
        PickedRight = false;
        won = false;
        
    }
    protected override void Update()
    {
        base.Update();
        if (currentTimer < 1 && !won)
        {
            won = true;
            Success();
        }
    }
    public void Clicked()
    {
        if (PickedRight == false)
        {
            elevator.SetActive(false);
            PickedRight = true;
            Fail();
        }
    }
    
    public override void OnDisable()
    {
        elevator.SetActive(false);
        base.OnDisable();
    }
    public override void ControlsUpdate()
    {
        
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        
    }
}
