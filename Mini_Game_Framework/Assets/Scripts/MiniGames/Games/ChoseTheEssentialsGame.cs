﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoseTheEssentialsGame : MiniGameBase {
    public List<GameObject> RightItems,WrongItems, allItems,spots;
    public int CorrectItems, IncorrectItems;
    public bool pickedRight, PickedWrong;
    public Animator CaseboxAnimator;
    public GameObject Casebox;
    public AddItems Case;
    
    public override void OnEnable()
    {
        base.OnEnable();
        CaseboxAnimator = Casebox.GetComponent<Animator>();
        var currentDiffData = currentDifficultyData as ChoseTheEssentialsDificulty;
        CorrectItems = currentDiffData.CorrectItems;
        IncorrectItems = currentDiffData.IncorrectItems;
        CreatItems();

    }
    public override void OnDisable()
    {
        for (int i = 0; i < Case.ItemsInside.Count; i++)
        {
            Case.ItemsInside[i].SetActive(false);
        }
        for (int i = 0; i < CorrectItems + IncorrectItems; i++)
        {
            Destroy(allItems[i]);
        }
        allItems.Clear();
       
        base.OnDisable();
    }
    public override void ControlsUpdate()
    {

    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {

        if (pickedRight)
        {
            CorrectItems--;
            pickedRight = false;
        }
        if (PickedWrong)
        {
            PickedWrong = false;
            Fail();
        }
    }
    protected override void Update()
    {
        base.Update();
        float animationPerc = CaseboxAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        bool isClosedAnimation = CaseboxAnimator.GetCurrentAnimatorStateInfo(0).IsName("Case_Closed");
   
        if (CorrectItems < 1)
        {
            PauseGame();
            CaseboxAnimator.SetTrigger("Closes");
        }
        if (isClosedAnimation && animationPerc >= 1)
        {
            Success();
        }
    }
    public void CreatItems()
    {
        int CorPos = CorrectItems;
        int Incorpos = IncorrectItems;
        for (int i = 0; i < CorrectItems+IncorrectItems; i++)
        {
            if (CorPos > 0)
            {
                if (Incorpos > 0)
                {
                    if (Random.Range(1, 3) < 2)
                    {
                        var CreatItem = Instantiate(RightItems[CorPos]) as GameObject;
                        CreatItem.transform.position = spots[i].transform.position;
                        CreatItem.transform.name = RightItems[CorPos].name;
                        CorPos--;
                        allItems.Add(CreatItem);
                    }
                    else
                    {
                        var CreatItem = Instantiate(WrongItems[Incorpos]) as GameObject;
                        CreatItem.transform.position = spots[i].transform.position;
                        CreatItem.transform.name = WrongItems[Incorpos].name;
                        Incorpos--;
                        allItems.Add(CreatItem);
                    }
                }
                else
                {
                    var CreatItem = Instantiate(RightItems[CorPos]) as GameObject;
                    CreatItem.transform.position = spots[i].transform.position;
                    CreatItem.transform.name = RightItems[CorPos].name;
                    CorPos--;
                    allItems.Add(CreatItem);
                }
            }
            else if (Incorpos > 0)
            {
                var CreatItem = Instantiate(WrongItems[Incorpos]) as GameObject;
                CreatItem.transform.position = spots[i].transform.position;
                CreatItem.transform.name = WrongItems[Incorpos].name;
                Incorpos--;
                allItems.Add(CreatItem);
            }
        }
    }
    public void ClearItems()
    {
        for (int i = 0; i < CorrectItems + IncorrectItems; i++)
        {
            Destroy(allItems[i]);
        }
        allItems.Clear();

    }
    public void PickedRight(string name)
    {
        pickedRight = true;
        for (int i = 0; i < allItems.Count; i++)
        {
            if (name == allItems[i].name)
            {
                allItems.RemoveAt(i);
            }
        }
    }
    public void pickedWrong()
    {
        PickedWrong = true;
    }
}
