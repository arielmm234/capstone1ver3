﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dial911Game : MiniGameBase
{
    public List<int> nineoneoneNumberSequence;
    public Transform ButtonsParent;
    public override void OnEnable()
    {
        base.OnEnable();
        //var currentDiffData = currentDifficultyData as Dial911GameDificulty;
        nineoneoneNumberSequence.Add(9);
        nineoneoneNumberSequence.Add(1);
        nineoneoneNumberSequence.Add(1);
       
        foreach (Transform child in ButtonsParent)
        {
            if (child.GetComponent<PhoneButton>())
                child.GetComponent<PhoneButton>().server = this;
        }
    }
    public override void OnDisable()
    {
        this.transform.parent.gameObject.SetActive(false);
        base.OnDisable();
    }
    public override void ControlsUpdate()
    {

    }
    public override void Reset()
    {
        base.Reset();
    }
    public void CompareNumber(int number)
    {

        if (number == nineoneoneNumberSequence[0])
        {
            nineoneoneNumberSequence.RemoveAt(0);
            nineoneoneNumberSequence.TrimExcess();
            if (nineoneoneNumberSequence.Count <= 0)
            {

                Success();
            }
        }
        else
        {
            nineoneoneNumberSequence.Clear();
            nineoneoneNumberSequence.Add(9);
            nineoneoneNumberSequence.Add(1);
            nineoneoneNumberSequence.Add(1);
        }
    }

}
