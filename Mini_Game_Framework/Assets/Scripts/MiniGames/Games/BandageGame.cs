﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BandageGame : MiniGameBase
{

    public GameObject Leg;
    public GameObject timer;
    public List<BandagePoint> Points = new List<BandagePoint>();
    public OofDetector OofDetector;
    LineRenderer lineRenderer;
    int lineRendererIndex;
    bool over;
    public override void OnEnable()
    {
        base.OnEnable();
        timer.SetActive(true);
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetPosition(0, Points[0].gameObject.transform.position);
        lineRenderer.SetPosition(1, Points[0].gameObject.transform.position);
        Points.ForEach(p => p.PointManager = this);
        for (int i = 1; i < Points.Count; i++)
            Points[i].gameObject.SetActive(false);
        lineRendererIndex = 1;
        over = true;
    }
    public override void OnDisable()
    {
        timer.SetActive(false);
        this.transform.parent.gameObject.SetActive(false);
        base.OnDisable();
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        if (Points.Count < 1)
        {
            Success();
        }

    }
    public void NextPoint(BandagePoint sender)
    {
        if (sender)
        {
            if (sender.TriggerTransparency)
            {
                Leg.GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0.4f);
                over = false;
            }
            else
            {
                Leg.GetComponent<Renderer>().material.color = Color.white;
                over = true;
            }
            if (sender.ConnectLine)
            {
                float distance = (lineRenderer.GetPosition(lineRendererIndex - 1) - sender.gameObject.transform.position).magnitude;
                Vector3 middle = Vector3.MoveTowards(lineRenderer.GetPosition(lineRendererIndex - 1), sender.gameObject.transform.position, distance / 2);
                Vector3 midmiddle = Vector3.MoveTowards(lineRenderer.GetPosition(lineRendererIndex - 1), middle, distance / 2);
                midmiddle.z = over ? 70 : 130f;
                lineRenderer.positionCount += lineRendererIndex == 1 ? 0 : 1;
                lineRenderer.SetPosition(lineRendererIndex, midmiddle);
                lineRendererIndex++;
                lineRenderer.positionCount++;
                lineRenderer.SetPosition(lineRendererIndex, Points[0].transform.position);
                lineRendererIndex++;
            }
            Points.RemoveAt(Points.FindIndex(p => p.Equals(sender)));
            //Destroy(sender.gameObject);
            sender.gameObject.SetActive(false);
            Points.TrimExcess();
            if (Points.Count > 0)
                Points[0].gameObject.SetActive(true);
        }
        else
        {

            Points.Clear();
            Points.TrimExcess();


        }
    }
    public void TooFast()
    {
        Fail();
    }

    protected override void Fail()
    {
        base.Fail();
        OofDetector.ResetGame();
        if (lineRenderer.positionCount > 2)
        {
            lineRenderer.positionCount = 2;
        }

        lineRenderer.SetPosition(0, Points[0].gameObject.transform.position);
        lineRenderer.SetPosition(1, Points[0].gameObject.transform.position);

        Points.ForEach(p => p.PointManager = this);
        for (int i = 1; i < Points.Count; i++)
            Points[i].gameObject.SetActive(false);
        Points[0].gameObject.SetActive(true);
        Debug.Log("FAILED");
        lineRendererIndex = 1;
    }
}
