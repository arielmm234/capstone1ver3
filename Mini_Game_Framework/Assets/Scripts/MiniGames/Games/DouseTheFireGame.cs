﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DouseTheFireGame : MiniGameBase
{
    public List<Sprite> CorImage, IncorImage;
    public int Rounds;
    public bool clickedRight;
    public bool clickedWrong;
    public GameObject canvas;
    public GameObject CorChoice, IncChoice;
    public List<GameObject> numbers;
    float xpos;
    public override void OnEnable()
    {
        base.OnEnable();
        var currentDiffData = currentDifficultyData as DouseTheFireDificulty;
        Camera.main.transform.position = new Vector3(0, 0, 0);
        canvas.GetComponent<Canvas>();
        Rounds = currentDiffData.Rounds;
        CreatChoices();
        clickedWrong = false;
        clickedRight = false;

    }
    public override void OnDisable()
    {
        base.OnDisable();
        if (numbers.Count > 0)
        {
            for (int i = 0; i < 3; i++)
            {
                Destroy(numbers[i]);
            }
            numbers.Clear();
        }
    }
    public override void ControlsUpdate()
    {

    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        if (clickedWrong && !GameOver)
        {
            Fail();
        }
        if (clickedRight)
        {
            clearImages();
            if (Rounds < 1)
            {
                Success();
            }
            else
            {
                CreatChoices();
                clickedRight = false;
                Rounds--;

            }

        }
    }
    public void ClickedCorrect()
    {
        clickedRight = true;
    }
    public void ClickedIncorrect()
    {
        clickedWrong = true;
    }
    public void CreatChoices()
    {
        int CorPos = 1;
        int Incorpos = 2;
        for (int i = 0; i < 3; i++)
        {
            if (i == 0)
            {
                xpos = canvas.transform.position.x * 0.25f;
            }
            if (i == 1)
            {
                xpos = canvas.transform.position.x;
            }
            if (i == 2)
            {
                xpos = canvas.transform.position.x * 1.75f;
            }
            if (CorPos > 0)
            {
                if (Incorpos > 0)
                {
                    if (Random.Range(1, 3) < 2)
                    {
                        SpawnImage(CorChoice, canvas, ref CorPos, xpos, CorImage);
                    }
                    else
                    {
                        SpawnImage(IncChoice, canvas, ref Incorpos, xpos, IncorImage);
                    }
                }
                else
                {
                    SpawnImage(CorChoice, canvas, ref CorPos, xpos, CorImage);
                }
            }
            else if (Incorpos > 0)
            {
                SpawnImage(IncChoice, canvas, ref Incorpos, xpos, IncorImage);
            }
        }
    }

    public void SpawnImage(GameObject choice, GameObject canvass, ref int typechoice, float Xposition, List<Sprite> Imagess)
    {
        GameObject creatImage = Instantiate(choice);
        creatImage.transform.SetParent(canvass.transform, false);
        creatImage.GetComponent<Image>().sprite = Imagess[Random.Range(0, Imagess.Count - 1)];
        creatImage.transform.position = new Vector3(Xposition, canvass.transform.position.y - 115.0f, canvass.transform.position.z);
        typechoice--;
        creatImage.transform.SetSiblingIndex(0);
        numbers.Add(creatImage);
    }
    public void clearImages()
    {
        for (int i = 0; i < 3; i++)
        {
            Destroy(numbers[i]);
        }
        numbers.Clear();
    }
}
