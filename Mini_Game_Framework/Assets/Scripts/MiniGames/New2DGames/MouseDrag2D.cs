﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag2D : MonoBehaviour {
    float distance = 100.0f;
    private void OnMouseDrag()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.position = objPosition;
    }
    
}
