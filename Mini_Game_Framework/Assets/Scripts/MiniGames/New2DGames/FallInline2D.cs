﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallInline2D : MiniGameBase
{
    public List<GameObject> line, AddedPeople;
    public GameObject ManPrefab, WholeLine;
    public int NumOfPeople,LineAmount;
    bool walking;
    public Canvas mainCanvas;

    public override void OnEnable()
    {
        base.OnEnable();
        var currentDiffData = currentDifficultyData as FallInLineDificulty;
        NumOfPeople = currentDiffData.NumOfPeople;
        line[0].SetActive(true);
        line[1].SetActive(true);
        for (int i = 0; i < NumOfPeople; i++)
        {
            var Person = Instantiate(ManPrefab);
            Person.transform.SetParent(mainCanvas.transform);
            Person.transform.localScale = new Vector3(1, 1, 1);
            Person.transform.localPosition = new Vector3(Random.Range(0, 634), Random.Range(-205, 205),100);
            AddedPeople.Add(Person);
        }

    }
    public override void OnDisable()
    {
        base.OnDisable();
        LineAmount = 0;
        for (int i = 0; i < AddedPeople.Count; i++)
        {
            Destroy(AddedPeople[i]);
        }
        AddedPeople.Clear();
        for (int i = 0; i < line.Count; i++)
        {
            line[i].SetActive(false);
        }
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (HasStarted && !GameOver)
        {
            if (LineAmount == NumOfPeople)
            {
                walking = true;
                PauseGame();
            }

        }
    }
    protected override void Update()
    {
        base.Update();
        if (walking)
        {
            WholeLine.transform.position += new Vector3(0,-0.41f,0);
        }
        if (WholeLine.transform.localPosition.y <= -433)
        {
            walking = false;
            Success();
        }
    }
    protected override void Success()
    {
        base.Success();
    }
}
