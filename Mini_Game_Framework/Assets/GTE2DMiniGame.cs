﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GTE2DMiniGame : MiniGameBase
{
    public List<GameObject> RightItems, WrongItems, allItems, spots, CorrectBagItems;
    public int CorrectItems, IncorrectItems;
    public Canvas mainCanvas;

    public bool pickedRight, PickedWrong;


    public override void OnEnable()
    {
        base.OnEnable();
        var currentDiffData = currentDifficultyData as ChoseTheEssentialsDificulty;
        CorrectItems = currentDiffData.CorrectItems;
        IncorrectItems = currentDiffData.IncorrectItems;
        Spawnitems();
    }
    public override void OnDisable()
    {
        base.OnDisable();
        for (int i = 0; i < CorrectBagItems.Count; i++)
        {
            CorrectBagItems[i].SetActive(false);
        }
        for (int i = 0; i < allItems.Count; i++)
        {
            Destroy(allItems[i]);
        }
        allItems.Clear();
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
        if (pickedRight)
        {
            CorrectBagItems[CorrectItems].SetActive(true);
            CorrectItems--;
            pickedRight = false;
        }
        if (PickedWrong)
        {
            PickedWrong = false;
            Fail();
        }
        if (CorrectItems < 1)
        {
            Success();
        }
    }
    protected override void Success()
    {
        base.Success();
    }
    protected override void Update()
    {
        base.Update();
    }

    public void Spawnitems()
    {
        int CorPos = CorrectItems;
        int Incorpos = IncorrectItems;
        for (int i = 0; i < CorrectItems + IncorrectItems; i++)
        {
            if (CorPos > 0)
            {
                if (Incorpos > 0)
                {
                    if (Random.Range(1, 3) < 2)
                    {
                        var CreatItem = Instantiate(RightItems[0]) as GameObject;
                        CreatItem.transform.SetParent(mainCanvas.transform);
                        CreatItem.transform.position = spots[i].transform.position;
                        CreatItem.transform.localScale = new Vector3(.5f, .5f, .5f);
                        CreatItem.transform.name = RightItems[0].name;
                        CorPos--;
                        allItems.Add(CreatItem);
                    }
                    else
                    {
                        var CreatItem = Instantiate(WrongItems[0]) as GameObject;
                        CreatItem.transform.SetParent(mainCanvas.transform);
                        CreatItem.transform.position = spots[i].transform.position;
                        CreatItem.transform.localScale = new Vector3(.5f, .5f, .5f);
                        CreatItem.transform.name = WrongItems[0].name;
                        Incorpos--;
                        allItems.Add(CreatItem);
                    }
                }
                else
                {
                    var CreatItem = Instantiate(RightItems[0]) as GameObject;
                    CreatItem.transform.SetParent(mainCanvas.transform);
                    CreatItem.transform.position = spots[i].transform.position;
                    CreatItem.transform.localScale = new Vector3(.5f, .5f, .5f);
                    CreatItem.transform.name = RightItems[0].name;
                    CorPos--;
                    allItems.Add(CreatItem);
                }
            }
            else if (Incorpos > 0)
            {
                var CreatItem = Instantiate(WrongItems[0]) as GameObject;
                CreatItem.transform.SetParent(mainCanvas.transform);
                CreatItem.transform.position = spots[i].transform.position;
                CreatItem.transform.localScale = new Vector3(.5f, .5f, .5f);
                CreatItem.transform.name = WrongItems[0].name;
                Incorpos--;
                allItems.Add(CreatItem);
            }
        }
    }
    public void PickedRight()
    {
        pickedRight = true;
    }
    public void pickedWrong()
    {
        PickedWrong = true;
    }
}
