﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialInteract : MonoBehaviour
{
    [SerializeField] float TurnSpeed;
    Transform objectInteract = null;
    TouchInput.TouchData touchData = new TouchInput.TouchData();
    public bool IsDragTutorial;
    public bool IsTapTutorial;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
     
        
        RotateLockerDoor();
        GetItems();
    }

    void GetItems()
    {
        if (!IsTapTutorial)
            return;

        touchData = TouchInput.GetTouch(0);


        if (TouchInput.OnTouchDown())
        {
            Ray ray = Camera.main.ScreenPointToRay(touchData.TouchPosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.CompareTag("ItemTutorialTrigger"))
                {
                    hit.collider.gameObject.SetActive(false);
                    if (!TutorialManager.Instance.ObjectsToInteract.Exists((x) => x.activeInHierarchy))
                    {
                        TutorialManager.Instance.IsCurrentTutorialDone = true;
                        IsTapTutorial = false;
                    }
                }
            }
         
        }
    }

    void RotateLockerDoor()
    {
        if (!IsDragTutorial)
            return;

        TouchInput.SwipeData swipe = TouchInput.OnSwipe();

        if (swipe.Touch != null)
        {

            if (swipe.Phase == TouchInput.SwipePhase.Start)
            {

            }
            if (swipe.Phase == TouchInput.SwipePhase.Swiping)
            {
                if (objectInteract == null)
                {
                    Ray ray = Camera.main.ScreenPointToRay(swipe.StartPosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                    {
                        if (hit.collider.CompareTag("TutorialTrigger"))
                        {
                            objectInteract = hit.transform.parent;
                            //if (swipe.Direction.x > 0)
                            //{
                            //    objectInteract.Rotate(Vector3.up, -TurnSpeed * Time.deltaTime);
                            //    Debug.Log("Closing locker");
                            //}
                            //else if (swipe.Direction.x < 0)
                            //{
                            //    objectInteract.Rotate(Vector3.up, TurnSpeed * Time.deltaTime);
                            //    Debug.Log("Opening locker");
                            //}
                        }
                    }
                }
                if (objectInteract != null)
                {
                    {
                        if (swipe.Direction.x > 0)
                        {

                            objectInteract.Rotate(Vector3.up, -TurnSpeed * Time.deltaTime);
                            
                        }
                        else if (swipe.Direction.x < 0)
                        {

                            objectInteract.Rotate(Vector3.up, TurnSpeed * Time.deltaTime);
                        }
                    }

                    objectInteract.localEulerAngles = new Vector3(objectInteract.localEulerAngles.x, ClampAngle(objectInteract.localEulerAngles.y, 0, 90), objectInteract.localEulerAngles.z);
                    if (objectInteract.localEulerAngles.y >= 90)
                    {
                        TutorialManager.Instance.IsCurrentTutorialDone = true;
                        IsDragTutorial = false;
                    }
                }

            }
            if (swipe.Phase == TouchInput.SwipePhase.Finished)
            {
                objectInteract = null;
            }
        }
    }

    protected float ClampAngle(float angle, float min, float max)
    {

        angle = NormalizeAngle(angle);
        if (angle > 180)
        {
            angle -= 360;
        }
        else if (angle < -180)
        {
            angle += 360;
        }

        min = NormalizeAngle(min);
        if (min > 180)
        {
            min -= 360;
        }
        else if (min < -180)
        {
            min += 360;
        }

        max = NormalizeAngle(max);
        if (max > 180)
        {
            max -= 360;
        }
        else if (max < -180)
        {
            max += 360;
        }

        // Aim is, convert angles to -180 until 180.
        return Mathf.Clamp(angle, min, max);
    }

    /** If angles over 360 or under 360 degree, then normalize them.
     */
    protected float NormalizeAngle(float angle)
    {
        while (angle > 360)
            angle -= 360;
        while (angle < 0)
            angle += 360;
        return angle;
    }
}
