﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duck2D : MiniGameBase
{
    private bool standUpDelayFinished;
    private bool isCameraShaking;

    public override void OnEnable()
    {
        base.OnEnable();
        var currentDiffData = currentDifficultyData as DuckGameDificulty;
        //timeCanStandWhileQuaking = currentDiffData.timeCanStandWhileQuaking;
        //Earthquack = currentDiffData.Earthquack;
        //Quakingtime = currentDiffData.Quakingtime;
        //playerspeed = currentDiffData.playerSpeed;
    }
    public override void OnDisable()
    {
        base.OnDisable();
    }

    public override void ControlsUpdate()
    {
        base.ControlsUpdate();
    }
    public override void Reset()
    {
        base.Reset();
    }
    public override void RulesUpdate()
    {
        base.RulesUpdate();
    }
    private IEnumerator StandUpDelay()
    {
        standUpDelayFinished = false;
        yield return new WaitForSeconds(0.1f);
        standUpDelayFinished = true;
    }

    private IEnumerator _ProcessShake(float shakeIntensity, float shakeTiming)
    {
        isCameraShaking = true;
        Noise(0.25f, shakeIntensity);
        yield return new WaitForSeconds(shakeTiming);
        Noise(0, 0);
        isCameraShaking = false;
    }

    public void Noise(float amplitudeGain, float frequencyGain)
    {
        MiniGameCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = amplitudeGain;
        MiniGameCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_FrequencyGain = frequencyGain;

    }
}
