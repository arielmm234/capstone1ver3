﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagAdder : MonoBehaviour {
    public GTE2DMiniGame game;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Correct")
        {
            game.PickedRight();
            Destroy(collision.gameObject);
        }
        if (collision.tag == "Incorrect")
        {
            game.pickedWrong();
            Destroy(collision.gameObject);
        }
    }
}
